import { MigrationInterface, QueryRunner, Table } from "typeorm";

export class CreateDoctorToken1649333809017 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: "doctor_token",
        columns: [
          {
            name: "id",
            type: "uuid",
            isPrimary: true,
          },
          {
            name: "doctor_id",
            type: "uuid",
          },
          {
            name: "refresh_token",
            type: "varchar",
            length: "2000",
          },
          {
            name: "expires_date",
            type: "timestamp with time zone",
          },
          {
            name: "created_at",
            type: "timestamp with time zone",
            default: "now()",
          },
        ],
        foreignKeys: [
          {
            name: "FkDoctorToken",
            referencedTableName: "doctor",
            referencedColumnNames: ["id"],
            columnNames: ["doctor_id"],
            onDelete: "CASCADE",
            onUpdate: "CASCADE",
          },
        ],
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable("doctor_token");
  }
}

import { MigrationInterface, QueryRunner, Table } from "typeorm";

export class CreateAppointment1649508271319 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: "appointment",
        columns: [
          {
            name: "id",
            type: "uuid",
            isPrimary: true,
          },
          {
            name: "doctor_id",
            type: "uuid",
          },
          {
            name: "patient_id",
            type: "uuid",
          },
          {
            name: "date",
            type: "timestamp",
          },
          {
            name: "comment",
            type: "text",
            isNullable: true,
          },
          {
            name: "active",
            type: "boolean",
            default: true,
          },
          {
            name: "created_at",
            type: "timestamp with time zone",
            default: "now()",
          },
          {
            name: "updated_at",
            type: "timestamp with time zone",
            default: "now()",
          },
        ],
        foreignKeys: [
          {
            name: "FkDoctorAppointment",
            referencedTableName: "doctor",
            referencedColumnNames: ["id"],
            columnNames: ["doctor_id"],
            onDelete: "SET NULL",
            onUpdate: "SET NULL",
          },
          {
            name: "FkPatientAppointment",
            referencedTableName: "patient",
            referencedColumnNames: ["id"],
            columnNames: ["patient_id"],
            onDelete: "SET NULL",
            onUpdate: "SET NULL",
          },
        ],
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable("appointment");
  }
}

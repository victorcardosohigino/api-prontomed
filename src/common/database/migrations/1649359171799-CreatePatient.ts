import { MigrationInterface, QueryRunner, Table } from "typeorm";

export class CreatePatient1649359171799 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: "patient",
        columns: [
          {
            name: "id",
            type: "uuid",
            isPrimary: true,
          },
          {
            name: "doctor_id",
            type: "uuid",
          },
          {
            name: "name",
            type: "varchar",
            length: "250",
          },
          {
            name: "phone",
            type: "varchar",
            length: "16",
          },
          {
            name: "email",
            type: "varchar",
            length: "250",
          },
          {
            name: "birth_date",
            type: "date",
          },
          {
            name: "gender",
            type: "varchar",
            length: "1",
          },
          {
            name: "height",
            type: "float",
          },
          {
            name: "weight",
            type: "float",
          },
          {
            name: "active",
            type: "boolean",
            default: true,
          },
          {
            name: "created_at",
            type: "timestamp with time zone",
            default: "now()",
          },
          {
            name: "updated_at",
            type: "timestamp with time zone",
            default: "now()",
          },
        ],
        foreignKeys: [
          {
            name: "FkDoctorPatient",
            referencedTableName: "doctor",
            referencedColumnNames: ["id"],
            columnNames: ["doctor_id"],
            onDelete: "SET NULL",
            onUpdate: "SET NULL",
          },
        ],
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable("patient");
  }
}

import { container } from "tsyringe";

import { IDateProvider } from "@common/provider/DateProvider/IDateProvider";
import { DayjsDateProvider } from "@common/provider/DateProvider/implementation/DayjsDateProvider";

container.registerSingleton<IDateProvider>(
  "DayjsDateProvider",
  DayjsDateProvider
);

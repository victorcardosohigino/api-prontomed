import { container } from "tsyringe";

import { IAppointmentRepository } from "@domain/appointment/repository/IAppointmentRepository";
import { AppointmentRepository } from "@domain/appointment/repository/implementation/AppointmentRepository";
import { IDoctorRepository } from "@domain/doctor/repository/IDoctorRepository";
import { IDoctorTokenRepository } from "@domain/doctor/repository/IDoctorTokenRepository";
import { DoctorRepository } from "@domain/doctor/repository/implementation/DoctorRepository";
import { DoctorTokenRepository } from "@domain/doctor/repository/implementation/DoctorTokenRepository";
import { PatientRepository } from "@domain/patient/repository/implementation/PatientRepository";
import { IPatientRepository } from "@domain/patient/repository/IPatientRepository";

container.registerSingleton<IDoctorRepository>(
  "DoctorRepository",
  DoctorRepository
);

container.registerSingleton<IDoctorTokenRepository>(
  "DoctorTokenRepository",
  DoctorTokenRepository
);

container.registerSingleton<IPatientRepository>(
  "PatientRepository",
  PatientRepository
);

container.registerSingleton<IAppointmentRepository>(
  "AppointmentRepository",
  AppointmentRepository
);

import { NextFunction, Request, Response } from "express";
import { verify } from "jsonwebtoken";

import { AppError } from "@common/error/AppError";
import auth from "@configuration/auth";

interface IPayload {
  sub: string;
}

export async function authenticated(
  request: Request,
  response: Response,
  next: NextFunction
) {
  const authHeader = request.headers.authorization;

  if (!authHeader) {
    throw new AppError("Token missing", 401);
  }

  const [, accessToken] = authHeader.split(" ");

  try {
    const { sub: doctor_id } = verify(
      accessToken,
      auth.secret_token
    ) as IPayload;

    request.doctor = {
      doctor_id,
    };

    next();
  } catch {
    throw new AppError("Invalid token!", 401);
  }
}

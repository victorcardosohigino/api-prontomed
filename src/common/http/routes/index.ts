import { Router } from "express";

import { appointmentRoutes } from "./appointment.routes";
import { authenticateRoutes } from "./authenticate.routes";
import { doctorRoutes } from "./doctor.routes";
import { patientRoutes } from "./patient.routes";

const router = Router();

router.get("/", (request, response) => {
  response.send(`<h1>Wellcome!</h1>`);
});

router.use("/doctor", doctorRoutes);
router.use("/authentication", authenticateRoutes);
router.use("/patient", patientRoutes);
router.use("/appointment", appointmentRoutes);

export { router };

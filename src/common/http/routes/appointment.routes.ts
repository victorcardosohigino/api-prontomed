import { celebrate, Segments, Joi } from "celebrate";
import { Router } from "express";

import { authenticated } from "@common/http/middleware/authenticated";
import { CreateAppointmentController } from "@domain/appointment/controller/CreateAppointmentController";
import { DeleteAppointmentController } from "@domain/appointment/controller/DeleteAppointmentController";
import { EditAppointmentController } from "@domain/appointment/controller/EditAppointmentController";
import { FindAppointmentController } from "@domain/appointment/controller/FindAppointmentController";
import { ListAppointmentController } from "@domain/appointment/controller/ListAppointmentController";
import { ListPatientAppointmentCommentController } from "@domain/appointment/controller/ListPatientAppointmentCommentController";

const appointmentRoutes = Router();

const createAppointmentController = new CreateAppointmentController();
const findAppointmentController = new FindAppointmentController();
const editAppointmentController = new EditAppointmentController();
const deleteAppointmentController = new DeleteAppointmentController();
const listPatientAppointmentCommentController =
  new ListPatientAppointmentCommentController();
const listAppointmentController = new ListAppointmentController();

appointmentRoutes.post(
  "/",
  authenticated,
  celebrate({
    [Segments.BODY]: {
      patient_id: Joi.string().uuid().required(),
      date: Joi.date().iso().required(),
      comment: Joi.string().allow("", null),
    },
  }),
  createAppointmentController.handle
);

appointmentRoutes.get(
  "/",
  authenticated,
  celebrate({
    [Segments.QUERY]: {
      patient_id: Joi.string().uuid(),
      date: Joi.date().iso(),
    },
  }),
  findAppointmentController.handle
);

appointmentRoutes.put(
  "/:id",
  authenticated,
  celebrate({
    [Segments.PARAMS]: {
      id: Joi.string().uuid().required(),
    },
    [Segments.BODY]: {
      patient_id: Joi.string().uuid().required(),
      date: Joi.date().iso().required(),
      comment: Joi.string().allow("", null),
    },
  }),
  editAppointmentController.handle
);

appointmentRoutes.delete(
  "/:id",
  authenticated,
  celebrate({
    [Segments.PARAMS]: {
      id: Joi.string().uuid().required(),
    },
  }),
  deleteAppointmentController.handle
);

appointmentRoutes.get(
  "/comments",
  authenticated,
  celebrate({
    [Segments.QUERY]: {
      patient_id: Joi.string().uuid().required(),
    },
  }),
  listPatientAppointmentCommentController.handle
);

appointmentRoutes.get(
  "/list",
  authenticated,
  celebrate({
    [Segments.QUERY]: {
      patient_id: Joi.string().uuid(),
      offset: Joi.number().integer(),
      limit: Joi.number().integer(),
    },
  }),
  listAppointmentController.handle
);

export { appointmentRoutes };

import { celebrate, Segments, Joi } from "celebrate";
import { Router } from "express";

import { CreateDoctorController } from "@domain/doctor/controller/CreateDoctorController";

const doctorRoutes = Router();

const createDoctorController = new CreateDoctorController();

doctorRoutes.post(
  "/",
  celebrate({
    [Segments.BODY]: {
      name: Joi.string().required().max(250),
      email: Joi.string().email().required().max(250),
      password: Joi.string().required().min(6),
    },
  }),
  createDoctorController.handle
);

export { doctorRoutes };

import { celebrate, Segments, Joi } from "celebrate";
import { Router } from "express";

import { authenticated } from "@common/http/middleware/authenticated";
import { CreatePatientController } from "@domain/patient/controller/CreatePatientController";
import { DeletePatientController } from "@domain/patient/controller/DeletePatientController";
import { DeletePatientKeepingRecordController } from "@domain/patient/controller/DeletePatientKeepingRecordController";
import { EditPatientController } from "@domain/patient/controller/EditPatientController";
import { FindPatientController } from "@domain/patient/controller/FindPatientController";
import { ListPatientController } from "@domain/patient/controller/ListPatientController";

const patientRoutes = Router();

const createPatientController = new CreatePatientController();
const findPatientController = new FindPatientController();
const editPatientController = new EditPatientController();
const deletePatientController = new DeletePatientController();
const deletePatientKeepingRecordController =
  new DeletePatientKeepingRecordController();
const listPatientController = new ListPatientController();

patientRoutes.post(
  "/",
  authenticated,
  celebrate({
    [Segments.BODY]: {
      name: Joi.string().required().max(250),
      phone: Joi.string().required().length(16),
      email: Joi.string().email().required().max(250),
      birth_date: Joi.date().iso().required(),
      gender: Joi.string().required().length(1).valid("M", "F", "N"),
      height: Joi.number().positive().required().max(400),
      weight: Joi.number().positive().required().max(500),
    },
  }),
  createPatientController.handle
);

patientRoutes.get(
  "/",
  authenticated,
  celebrate({
    [Segments.QUERY]: {
      name: Joi.string().max(250),
      phone: Joi.string().max(16),
      email: Joi.string().email().max(250),
    },
  }),
  findPatientController.handle
);

patientRoutes.put(
  "/:id",
  authenticated,
  celebrate({
    [Segments.PARAMS]: {
      id: Joi.string().uuid().required(),
    },
    [Segments.BODY]: {
      name: Joi.string().required().max(250),
      phone: Joi.string().required().length(16),
      email: Joi.string().email().required().max(250),
      birth_date: Joi.date().iso().required(),
      gender: Joi.string().required().length(1).valid("M", "F", "N"),
      height: Joi.number().positive().required().max(400),
      weight: Joi.number().positive().required().max(500),
    },
  }),
  editPatientController.handle
);

patientRoutes.delete(
  "/:id",
  authenticated,
  celebrate({
    [Segments.PARAMS]: {
      id: Joi.string().uuid().required(),
    },
  }),
  deletePatientController.handle
);

patientRoutes.delete(
  "/:id/keeping-records",
  authenticated,
  celebrate({
    [Segments.PARAMS]: {
      id: Joi.string().uuid().required(),
    },
  }),
  deletePatientKeepingRecordController.handle
);

patientRoutes.get(
  "/list",
  authenticated,
  celebrate({
    [Segments.QUERY]: {
      offset: Joi.number().integer(),
      limit: Joi.number().integer(),
    },
  }),
  listPatientController.handle
);

export { patientRoutes };

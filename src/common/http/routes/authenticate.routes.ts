import { celebrate, Segments, Joi } from "celebrate";
import { Router } from "express";

import { AuthenticateDoctorController } from "@domain/doctor/controller/AuthenticateDoctorController";
import { RefreshTokenController } from "@domain/doctor/controller/RefreshTokenController";

const authenticateRoutes = Router();

const authenticateDoctorController = new AuthenticateDoctorController();
const refreshTokenController = new RefreshTokenController();

authenticateRoutes.post(
  "/session",
  celebrate({
    [Segments.BODY]: {
      email: Joi.string().email().required(),
      password: Joi.string().required(),
    },
  }),
  authenticateDoctorController.handle
);

authenticateRoutes.post(
  "/refresh-token",
  celebrate({
    [Segments.BODY]: {
      refresh_token: Joi.string().required(),
    },
  }),
  refreshTokenController.handle
);

export { authenticateRoutes };

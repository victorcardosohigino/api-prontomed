import "reflect-metadata";
import "dotenv/config";
import { errors } from "celebrate";
import cors from "cors";
import express, { NextFunction, Request, Response } from "express";
import "express-async-errors";
import "@common/container";
import swaggerUi from "swagger-ui-express";

import createConnection from "@common/database/connection";
import { AppError } from "@common/error/AppError";

import documentatioFile from "../../documentation/api-documentation.json";
import { router } from "./routes";

createConnection();
const app = express();

app.use(express.json());

app.use(
  "/api-documentation",
  swaggerUi.serve,
  swaggerUi.setup(documentatioFile)
);

app.use(cors());
app.use(router);

app.use(errors());

app.use(
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  (err: Error, request: Request, response: Response, next: NextFunction) => {
    if (err instanceof AppError) {
      return response.status(err.statusCode).json({
        message: err.message,
      });
    }

    return response.status(500).json({
      status: "error",
      message: `Internal server error - ${err.message}`,
    });
  }
);

export { app };

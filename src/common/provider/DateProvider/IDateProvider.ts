interface IDateProvider {
  compareInMinutes(start_date: Date, end_date: Date): number;
  convertToUTC(date: Date): string;
  currentDate(): Date;
  addDays(days: number): Date;
  addMinutesToDate(date: Date, minutes: number): Date;
  subtractMinutesToDate(date: Date, minutes: number): Date;
  compareIfBefore(start_date: Date, end_date: Date): boolean;
  convertToDate(date: string): Date;
  convertToTimezone(date: Date): Date;
}

export { IDateProvider };

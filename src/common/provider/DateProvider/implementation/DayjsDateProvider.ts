import dayjs from "dayjs";
import timezone from "dayjs/plugin/timezone";
import utc from "dayjs/plugin/utc";

import { IDateProvider } from "@common/provider/DateProvider/IDateProvider";

dayjs.extend(utc);
dayjs.extend(timezone);

class DayjsDateProvider implements IDateProvider {
  compareInMinutes(start_date: Date, end_date: Date): number {
    const endDateUtc = this.convertToUTC(end_date);
    const startDateUtc = this.convertToUTC(start_date);
    if (this.compareIfBefore(start_date, end_date)) {
      return dayjs(endDateUtc).diff(startDateUtc, "minutes");
    }
    return dayjs(startDateUtc).diff(endDateUtc, "minutes");
  }

  convertToUTC(date: Date): string {
    return dayjs(date).utc().local().format();
  }

  currentDate(): Date {
    return dayjs().toDate();
  }

  addDays(days: number): Date {
    return dayjs().add(days, "days").toDate();
  }

  compareIfBefore(start_date: Date, end_date: Date): boolean {
    return dayjs(start_date).isBefore(end_date);
  }

  addMinutesToDate(date: Date, minutes: number): Date {
    return dayjs(date).utc(true).add(minutes, "minutes").toDate();
  }

  subtractMinutesToDate(date: Date, minutes: number): Date {
    return dayjs(date).utc(true).subtract(minutes, "minutes").toDate();
  }

  convertToTimezone(date: Date): Date {
    return dayjs(date).utc(true).toDate();
  }

  convertToDate(date: string): Date {
    return dayjs(date).utc(true).toDate();
  }
}

export { DayjsDateProvider };

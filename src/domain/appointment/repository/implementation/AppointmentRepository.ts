import {
  Between,
  FindManyOptions,
  getRepository,
  Not,
  Repository,
} from "typeorm";

import { ICreateAppointmentDTO } from "@domain/appointment/dto/ICreateAppointmentDTO";
import { IFindByDoctorAndBetweenDateDTO } from "@domain/appointment/dto/IFindByDoctorAndBetweenDateDTO";
import { IFindByDoctorAndDateDTO } from "@domain/appointment/dto/IFindByDoctorAndDateDTO";
import { IFindByDoctorIdOrPatientIdOrDateDTO } from "@domain/appointment/dto/IFindByDoctorIdOrPatientIdOrDateDTO";
import { IFindByIdAndDoctorAppointmentDTO } from "@domain/appointment/dto/IFindByIdAndDoctorAppointmentDTO";
import { IListAppointmentDTO } from "@domain/appointment/dto/IListAppointmentDTO";
import { IListPatientAppointmentCommentDTO } from "@domain/appointment/dto/IListPatientAppointmentCommentDTO";
import { Appointment } from "@domain/appointment/entity/Appointment";
import { IAppointmentRepository } from "@domain/appointment/repository/IAppointmentRepository";

class AppointmentRepository implements IAppointmentRepository {
  private repository: Repository<Appointment>;

  constructor() {
    this.repository = getRepository(Appointment);
  }

  async findByDoctorAndDate({
    doctor_id,
    date,
  }: IFindByDoctorAndDateDTO): Promise<Appointment> {
    const appointment = await this.repository.findOne({
      where: {
        doctor_id,
        date,
        active: true,
      },
    });

    return appointment;
  }

  async findByDoctorAndBetweenDate({
    doctor_id,
    date_less,
    date_greater,
    appointment_id,
  }: IFindByDoctorAndBetweenDateDTO): Promise<Appointment> {
    const where = {
      doctor_id,
      active: true,
      date: Between(date_less, date_greater),
    };

    if (appointment_id) {
      Object.assign(where, { id: Not(appointment_id) });
    }

    const appointment = await this.repository.findOne({
      where,
      order: {
        date: "DESC",
      },
    });

    return appointment;
  }

  async findByDoctorAndBetweenDates({
    doctor_id,
    date_less,
    date_greater,
    appointment_id,
  }: IFindByDoctorAndBetweenDateDTO): Promise<Appointment[]> {
    const where = {
      doctor_id,
      active: true,
      date: Between(date_less, date_greater),
    };

    if (appointment_id) {
      Object.assign(where, { id: Not(appointment_id) });
    }

    const appointments = await this.repository.find({
      where,
      order: {
        date: "DESC",
      },
    });

    return appointments;
  }

  async create({
    doctor_id,
    patient_id,
    date,
    comment,
  }: ICreateAppointmentDTO): Promise<Appointment> {
    const appointment = this.repository.create({
      doctor_id,
      patient_id,
      date,
      comment,
    });

    await this.repository.save(appointment);

    return appointment;
  }

  async findById(id: string): Promise<Appointment> {
    const appointment = await this.repository.findOne({
      id,
      active: true,
    });
    return appointment;
  }

  async findByDoctor(doctor_id: string): Promise<Appointment> {
    const appointment = await this.repository.findOne({
      doctor_id,
      active: true,
    });
    return appointment;
  }

  async findByPatient(patient_id: string): Promise<Appointment[]> {
    const appointments = await this.repository.find({
      patient_id,
      active: true,
    });
    return appointments;
  }

  async findByDoctorAndPatientAndDate({
    doctor_id,
    patient_id,
    date,
  }: IFindByDoctorIdOrPatientIdOrDateDTO): Promise<Appointment[]> {
    const qb = this.repository
      .createQueryBuilder()
      .select([
        "id",
        "doctor_id",
        "patient_id",
        "date",
        "comment",
        "created_at",
        "updated_at",
        "active",
      ])
      .orderBy("date", "ASC")
      .where({
        doctor_id,
        active: true,
      });

    if (patient_id) {
      qb.andWhere("patient_id = :patient_id", { patient_id });
    }

    if (date) {
      qb.andWhere("date = :date", { date });
    }

    const appointments = await qb.getRawMany();

    return appointments;
  }

  async save(appointment: Appointment): Promise<void> {
    this.repository.save(appointment);
  }

  async findByIdAndDoctor({
    id,
    doctor_id,
  }: IFindByIdAndDoctorAppointmentDTO): Promise<Appointment> {
    const appointment = await this.repository.findOne({
      id,
      doctor_id,
      active: true,
    });
    return appointment;
  }

  async delete(id: string): Promise<void> {
    this.repository.update({ id }, { active: false });
  }

  async findAppointmentCommentsByPatient({
    doctor_id,
    patient_id,
  }: IListPatientAppointmentCommentDTO): Promise<Appointment[]> {
    const where = {
      doctor_id,
      patient_id,
      active: true,
    };

    const findOptions = {
      select: ["id", "date", "comment", "created_at", "updated_at"],
      where,
      order: { date: "DESC" },
    } as FindManyOptions;

    const appointments = await this.repository.find(findOptions);

    return appointments;
  }

  async deleteKeepingRecord(patient_id: string): Promise<void> {
    this.repository.update(
      { patient_id },
      { comment: "deleted", active: false }
    );
  }

  async findAll({
    doctor_id,
    patient_id,
    offset,
    limit,
  }: IListAppointmentDTO): Promise<Appointment[]> {
    const where = {
      doctor_id,
      active: true,
    };

    if (patient_id) {
      Object.assign(where, {
        patient: {
          id: patient_id,
          active: true,
        },
      });
    }

    const findOptions = {
      where,
      order: { date: "ASC" },
    } as FindManyOptions;

    if (offset) {
      Object.assign(findOptions, { skip: offset });
    }

    if (limit) {
      Object.assign(findOptions, { take: limit });
    }

    const appointments = await this.repository.find(findOptions);

    return appointments;
  }
}

export { AppointmentRepository };

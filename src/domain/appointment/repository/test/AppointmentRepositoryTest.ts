import { DayjsDateProvider } from "@common/provider/DateProvider/implementation/DayjsDateProvider";
import { ICreateAppointmentDTO } from "@domain/appointment/dto/ICreateAppointmentDTO";
import { IFindByDoctorAndBetweenDateDTO } from "@domain/appointment/dto/IFindByDoctorAndBetweenDateDTO";
import { IFindByDoctorAndDateDTO } from "@domain/appointment/dto/IFindByDoctorAndDateDTO";
import { IFindByDoctorIdOrPatientIdOrDateDTO } from "@domain/appointment/dto/IFindByDoctorIdOrPatientIdOrDateDTO";
import { IFindByIdAndDoctorAppointmentDTO } from "@domain/appointment/dto/IFindByIdAndDoctorAppointmentDTO";
import { IListAppointmentDTO } from "@domain/appointment/dto/IListAppointmentDTO";
import { IListPatientAppointmentCommentDTO } from "@domain/appointment/dto/IListPatientAppointmentCommentDTO";
import { Appointment } from "@domain/appointment/entity/Appointment";
import { IAppointmentRepository } from "@domain/appointment/repository/IAppointmentRepository";

class AppointmentRepositoryTest implements IAppointmentRepository {
  appointments: Appointment[] = [];
  private dateProvider: DayjsDateProvider = new DayjsDateProvider();

  async findByDoctorAndDate({
    doctor_id,
    date,
  }: IFindByDoctorAndDateDTO): Promise<Appointment> {
    const findDate = this.dateProvider.convertToTimezone(date);
    const appointment = this.appointments.find(
      (appointmentTest) =>
        appointmentTest.date.getTime() === findDate.getTime() &&
        appointmentTest.doctor_id === doctor_id
    );

    return appointment;
  }

  async findByDoctorAndBetweenDate({
    doctor_id,
    date_less,
    date_greater,
  }: IFindByDoctorAndBetweenDateDTO): Promise<Appointment> {
    return this.appointments.find(
      (appointmentTest) =>
        appointmentTest.doctor_id === doctor_id &&
        appointmentTest.date >= new Date(date_less) &&
        appointmentTest.date <= new Date(date_greater)
    );
  }

  async create({
    doctor_id,
    patient_id,
    date,
    comment,
  }: ICreateAppointmentDTO): Promise<Appointment> {
    const appointment = new Appointment();

    Object.assign(appointment, {
      doctor_id,
      patient_id,
      date: this.dateProvider.convertToTimezone(date),
      comment,
    });

    this.appointments.push(appointment);

    return appointment;
  }

  async findById(id: string): Promise<Appointment> {
    const appointment = this.appointments.find(
      (appointmentTest) => appointmentTest.id === id
    );

    return appointment;
  }

  async findByDoctor(doctor_id: string): Promise<Appointment> {
    const appointment = this.appointments.find(
      (appointmentTest) => appointmentTest.doctor_id === doctor_id
    );

    return appointment;
  }

  async findByPatient(patient_id: string): Promise<Appointment[]> {
    const appointments = this.appointments.filter(
      (appointmentTest) => appointmentTest.patient_id === patient_id
    );
    return appointments;
  }

  async findByDoctorAndPatientAndDate({
    doctor_id,
    patient_id,
    date,
  }: IFindByDoctorIdOrPatientIdOrDateDTO): Promise<Appointment[]> {
    const dateConverted = this.dateProvider.convertToDate(date);

    let { appointments } = this;

    if (doctor_id) {
      appointments = appointments.filter(
        (appointmentTest) => appointmentTest.doctor_id === doctor_id
      );
    }

    if (patient_id) {
      appointments = appointments.filter(
        (appointmentTest) => appointmentTest.patient_id === patient_id
      );
    }

    if (date) {
      appointments = appointments.filter(
        (appointmentTest) =>
          appointmentTest.date.getTime() === dateConverted.getTime()
      );
    }

    return appointments;
  }

  async save(appointment: Appointment): Promise<void> {
    const index = this.appointments.findIndex(
      (appointmentIndex) => appointmentIndex.id === appointment.id
    );

    this.appointments[index] = appointment;
  }

  async findByIdAndDoctor({
    id,
    doctor_id,
  }: IFindByIdAndDoctorAppointmentDTO): Promise<Appointment> {
    const appointment = this.appointments.find(
      (appointmentTest) =>
        appointmentTest.id === id && appointmentTest.doctor_id === doctor_id
    );

    return appointment;
  }

  async delete(id: string): Promise<void> {
    const appointment = this.appointments.find(
      (appointmentTest) => appointmentTest.id === id
    );

    this.appointments.splice(this.appointments.indexOf(appointment));
  }

  async findAppointmentCommentsByPatient({
    doctor_id,
    patient_id,
  }: IListPatientAppointmentCommentDTO): Promise<Appointment[]> {
    const appointments = this.appointments.filter(
      (appointmentTest) =>
        appointmentTest.doctor_id === doctor_id &&
        appointmentTest.patient_id === patient_id
    );

    return appointments;
  }

  async deleteKeepingRecord(patient_id: string): Promise<void> {
    const appointments = this.appointments.filter(
      (appointmentInMemory) => appointmentInMemory.patient_id === patient_id
    );

    appointments.forEach((appointment, index) => {
      this.appointments[index].comment = null;
    });
  }

  async findAll({
    doctor_id,
    patient_id,
    offset,
    limit,
  }: IListAppointmentDTO): Promise<Appointment[]> {
    let appointments = this.appointments.filter(
      (appointmentTest) => appointmentTest.doctor_id === doctor_id
    );

    if (patient_id) {
      appointments = appointments.filter(
        (appointmentTest) => appointmentTest.patient_id === patient_id
      );
      if (offset && limit) {
        return appointments.slice(offset, limit + 1);
      }
    }

    if (offset && limit) {
      return appointments.slice(offset, limit + 1);
    }

    return appointments;
  }
}

export { AppointmentRepositoryTest };

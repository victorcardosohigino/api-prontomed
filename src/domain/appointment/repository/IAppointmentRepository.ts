import { ICreateAppointmentDTO } from "@domain/appointment/dto/ICreateAppointmentDTO";
import { IFindByDoctorAndBetweenDateDTO } from "@domain/appointment/dto/IFindByDoctorAndBetweenDateDTO";
import { IFindByDoctorAndDateDTO } from "@domain/appointment/dto/IFindByDoctorAndDateDTO";
import { IFindByDoctorIdOrPatientIdOrDateDTO } from "@domain/appointment/dto/IFindByDoctorIdOrPatientIdOrDateDTO";
import { IFindByIdAndDoctorAppointmentDTO } from "@domain/appointment/dto/IFindByIdAndDoctorAppointmentDTO";
import { IListAppointmentDTO } from "@domain/appointment/dto/IListAppointmentDTO";
import { IListPatientAppointmentCommentDTO } from "@domain/appointment/dto/IListPatientAppointmentCommentDTO";
import { Appointment } from "@domain/appointment/entity/Appointment";

interface IAppointmentRepository {
  findByDoctorAndDate(data: IFindByDoctorAndDateDTO): Promise<Appointment>;
  findByDoctorAndBetweenDate(
    data: IFindByDoctorAndBetweenDateDTO
  ): Promise<Appointment>;
  create(data: ICreateAppointmentDTO): Promise<Appointment>;
  findById(id: string): Promise<Appointment>;
  findByPatient(patient_id: string): Promise<Appointment[]>;
  findByDoctor(patient_id: string): Promise<Appointment>;
  findByDoctorAndPatientAndDate(
    data: IFindByDoctorIdOrPatientIdOrDateDTO
  ): Promise<Appointment[]>;
  save(appointment: Appointment): Promise<void>;
  findByIdAndDoctor(
    data: IFindByIdAndDoctorAppointmentDTO
  ): Promise<Appointment>;
  delete(id: string): Promise<void>;
  findAppointmentCommentsByPatient(
    data: IListPatientAppointmentCommentDTO
  ): Promise<Appointment[]>;
  deleteKeepingRecord(patient_id: string): Promise<void>;
  findAll(data: IListAppointmentDTO): Promise<Appointment[]>;
}

export { IAppointmentRepository };

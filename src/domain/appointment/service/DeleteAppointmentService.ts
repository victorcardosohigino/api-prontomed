import { inject, injectable } from "tsyringe";

import { AppError } from "@common/error/AppError";
import { IDeleteByIdAndDoctorAppointmentDTO } from "@domain/appointment/dto/IDeleteByIdAndDoctorAppointmentDTO";
import { IAppointmentRepository } from "@domain/appointment/repository/IAppointmentRepository";
import { IDoctorRepository } from "@domain/doctor/repository/IDoctorRepository";

@injectable()
class DeleteAppointmentService {
  constructor(
    @inject("AppointmentRepository")
    private appointmentRepository: IAppointmentRepository,
    @inject("DoctorRepository")
    private doctorRepository: IDoctorRepository
  ) {}

  async execute({
    id,
    doctor_id,
  }: IDeleteByIdAndDoctorAppointmentDTO): Promise<void> {
    const doctor = await this.doctorRepository.findById(doctor_id);

    if (!doctor) {
      throw new AppError("Doctor not found");
    }

    const appointment = await this.appointmentRepository.findByIdAndDoctor({
      id,
      doctor_id,
    });

    if (!appointment) {
      throw new AppError("Appointment not found");
    }

    this.appointmentRepository.delete(id);
  }
}

export { DeleteAppointmentService };

import { inject, injectable } from "tsyringe";

import { AppError } from "@common/error/AppError";
import { IFindByDoctorIdOrPatientIdOrDateDTO } from "@domain/appointment/dto/IFindByDoctorIdOrPatientIdOrDateDTO";
import { Appointment } from "@domain/appointment/entity/Appointment";
import { IAppointmentRepository } from "@domain/appointment/repository/IAppointmentRepository";
import { IDoctorRepository } from "@domain/doctor/repository/IDoctorRepository";
import { IPatientRepository } from "@domain/patient/repository/IPatientRepository";

@injectable()
class FindAppointmentService {
  constructor(
    @inject("AppointmentRepository")
    private appointmentRepository: IAppointmentRepository,
    @inject("DoctorRepository")
    private doctorRepository: IDoctorRepository,
    @inject("PatientRepository")
    private patientRepository: IPatientRepository
  ) {}

  async execute({
    doctor_id,
    patient_id,
    date,
  }: IFindByDoctorIdOrPatientIdOrDateDTO): Promise<Appointment[]> {
    if (doctor_id) {
      const doctor = await this.doctorRepository.findById(doctor_id);

      if (!doctor) {
        throw new AppError("Doctor not found");
      }
    }

    if (patient_id) {
      const patient = await this.patientRepository.findById(patient_id);

      if (!patient) {
        throw new AppError("Patient not found");
      }
    }

    const appointments =
      await this.appointmentRepository.findByDoctorAndPatientAndDate({
        doctor_id,
        patient_id,
        date,
      });

    return appointments;
  }
}

export { FindAppointmentService };

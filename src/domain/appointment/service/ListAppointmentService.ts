import { inject, injectable } from "tsyringe";

import { AppError } from "@common/error/AppError";
import { IListAppointmentDTO } from "@domain/appointment/dto/IListAppointmentDTO";
import { Appointment } from "@domain/appointment/entity/Appointment";
import { IAppointmentRepository } from "@domain/appointment/repository/IAppointmentRepository";
import { IPatientRepository } from "@domain/patient/repository/IPatientRepository";

@injectable()
class ListAppointmentService {
  constructor(
    @inject("AppointmentRepository")
    private appointmentRepository: IAppointmentRepository,
    @inject("PatientRepository")
    private patientRepository: IPatientRepository
  ) {}

  async execute({
    doctor_id,
    patient_id,
    offset,
    limit,
  }: IListAppointmentDTO): Promise<Appointment[]> {
    if (patient_id) {
      const patient = await this.patientRepository.findById(patient_id);

      if (!patient) {
        throw new AppError("Patient not found");
      }
    }

    const appointments = await this.appointmentRepository.findAll({
      doctor_id,
      patient_id,
      offset,
      limit,
    });

    return appointments;
  }
}

export { ListAppointmentService };

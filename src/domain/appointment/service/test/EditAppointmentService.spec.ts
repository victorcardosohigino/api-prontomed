import { v4 as uuid } from "uuid";

import { AppError } from "@common/error/AppError";
import { DayjsDateProvider } from "@common/provider/DateProvider/implementation/DayjsDateProvider";
import appointmentConfiguration from "@configuration/appointment";
import { Appointment } from "@domain/appointment/entity/Appointment";
import { AppointmentRepositoryTest } from "@domain/appointment/repository/test/AppointmentRepositoryTest";
import { CreateAppointmentService } from "@domain/appointment/service/CreateAppointmentService";
import { EditAppointmentService } from "@domain/appointment/service/EditAppointmentService";
import { Doctor } from "@domain/doctor/entity/Doctor";
import { DoctorRepositoryTest } from "@domain/doctor/repository/test/DoctorRepositoryTest";
import { CreateDoctorService } from "@domain/doctor/service/CreateDoctorService";
import { Patient } from "@domain/patient/entity/Patient";
import { PatientRepositoryTest } from "@domain/patient/repository/test/PatientRepositoryTest";
import { CreatePatientService } from "@domain/patient/service/CreatePatientService";

let patientRepositoryTest: PatientRepositoryTest;
let doctorRepositoryTest: DoctorRepositoryTest;
let appointmentRepositoryTest: AppointmentRepositoryTest;
let dateProvider: DayjsDateProvider;
let createDoctorService: CreateDoctorService;
let createPatientService: CreatePatientService;
let createAppointmentService: CreateAppointmentService;
let editAppointmentService: EditAppointmentService;

let appointment: Appointment;
let doctor: Doctor;
let patient: Patient;
let currentDate: Date;
let currentYear: number;
let currentMonth: number;
let currentDay: number;

describe("Edit Appointment", () => {
  beforeEach(async () => {
    dateProvider = new DayjsDateProvider();

    doctorRepositoryTest = new DoctorRepositoryTest();
    createDoctorService = new CreateDoctorService(doctorRepositoryTest);

    patientRepositoryTest = new PatientRepositoryTest();
    createPatientService = new CreatePatientService(
      patientRepositoryTest,
      doctorRepositoryTest
    );

    appointmentRepositoryTest = new AppointmentRepositoryTest();
    createAppointmentService = new CreateAppointmentService(
      appointmentRepositoryTest,
      patientRepositoryTest,
      dateProvider
    );

    editAppointmentService = new EditAppointmentService(
      appointmentRepositoryTest,
      patientRepositoryTest,
      dateProvider
    );

    const newDoctor = await createDoctorService.execute({
      name: "Doctor Test",
      email: "doctor@test.com",
      password: "123456",
    });

    doctor = newDoctor;

    const newPatient = await createPatientService.execute({
      doctor_id: doctor.id,
      name: "Patient Test",
      phone: "(48) 9 8875-0125",
      email: "patient@test.com",
      birth_date: "1985-07-16",
      gender: "M",
      height: 1.67,
      weight: 70.4,
    });

    patient = newPatient;

    currentDate = new Date();
    currentYear = currentDate.getFullYear();
    currentMonth = currentDate.getMonth() + 1;
    currentDay = currentDate.getDate();

    const appointmentDate = new Date(
      `${currentYear + 1}-${currentMonth}-${currentDay} 09:00:00`
    );

    appointment = await createAppointmentService.execute({
      doctor_id: doctor.id,
      patient_id: patient.id,
      date: appointmentDate,
    });
  });

  it("must edit a appointment", async () => {
    const appointmentDate = new Date(
      `${currentYear + 1}-${currentMonth}-${currentDay} 11:00:00`
    );

    const appointmentEdited = await editAppointmentService.execute({
      id: appointment.id,
      doctor_id: doctor.id,
      patient_id: patient.id,
      date: appointmentDate,
    });

    expect(appointmentEdited).toBeInstanceOf(Appointment);
    expect(appointmentEdited).toHaveProperty("id");
  });

  it("should not edit an appointment on a past date", async () => {
    const appointmentDate = new Date(
      `${currentYear - 1}-${currentMonth}-${currentDay} 09:00:00`
    );

    await expect(
      editAppointmentService.execute({
        id: appointment.id,
        doctor_id: doctor.id,
        patient_id: patient.id,
        date: appointmentDate,
      })
    ).rejects.toEqual(
      new AppError("The appointment date cannot be less than the current date")
    );
  });

  it("must edit an appointment with the same date to different doctor", async () => {
    const doctorTwo = await createDoctorService.execute({
      name: "Doctor Test Two",
      email: "doctor2@test.com",
      password: "123456",
    });

    const patientTwo = await createPatientService.execute({
      doctor_id: doctorTwo.id,
      name: "Patient Test two",
      phone: "(48) 9 8875-0126",
      email: "patient2@test.com",
      birth_date: "1985-07-16",
      gender: "M",
      height: 180,
      weight: 98.7,
    });

    const appointmentDate = new Date(
      `${currentYear + 1}-${currentMonth}-${currentDay} 14:00:00`
    );

    await createAppointmentService.execute({
      doctor_id: doctorTwo.id,
      patient_id: patientTwo.id,
      date: appointmentDate,
    });

    const appointmentEdited = await editAppointmentService.execute({
      id: appointment.id,
      doctor_id: doctor.id,
      patient_id: patient.id,
      date: appointmentDate,
    });

    expect(appointmentEdited).toBeInstanceOf(Appointment);
    expect(appointmentEdited).toHaveProperty("id");
  });

  it("should not edit an appointment with the same date interval", async () => {
    const appointmentDateTwo = new Date(
      `${currentYear + 1}-${currentMonth}-${currentDay} 09:15:00`
    );

    await expect(
      editAppointmentService.execute({
        id: appointment.id,
        doctor_id: doctor.id,
        patient_id: patient.id,
        date: appointmentDateTwo,
      })
    ).rejects.toEqual(
      new AppError(
        `The appointment must have a duration of ${appointmentConfiguration.duration} minutes`
      )
    );
  });

  it("should not edit an appointment to a non-existent patient", async () => {
    const appointmentDate = new Date(
      `${currentYear + 1}-${currentMonth}-${currentDay} 18:00:00`
    );

    await expect(
      editAppointmentService.execute({
        id: appointment.id,
        doctor_id: doctor.id,
        patient_id: uuid(),
        date: appointmentDate,
      })
    ).rejects.toEqual(new AppError("Patient not found"));
  });
});

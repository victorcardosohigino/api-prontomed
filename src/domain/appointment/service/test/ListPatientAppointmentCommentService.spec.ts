import { v4 as uuid } from "uuid";

import { AppError } from "@common/error/AppError";
import { DayjsDateProvider } from "@common/provider/DateProvider/implementation/DayjsDateProvider";
import { Appointment } from "@domain/appointment/entity/Appointment";
import { AppointmentRepositoryTest } from "@domain/appointment/repository/test/AppointmentRepositoryTest";
import { CreateAppointmentService } from "@domain/appointment/service/CreateAppointmentService";
import { ListPatientAppointmentCommentService } from "@domain/appointment/service/ListPatientAppointmentCommentService";
import { Doctor } from "@domain/doctor/entity/Doctor";
import { DoctorRepositoryTest } from "@domain/doctor/repository/test/DoctorRepositoryTest";
import { CreateDoctorService } from "@domain/doctor/service/CreateDoctorService";
import { Patient } from "@domain/patient/entity/Patient";
import { PatientRepositoryTest } from "@domain/patient/repository/test/PatientRepositoryTest";
import { CreatePatientService } from "@domain/patient/service/CreatePatientService";

let patientRepositoryTest: PatientRepositoryTest;
let doctorRepositoryTest: DoctorRepositoryTest;
let appointmentRepositoryTest: AppointmentRepositoryTest;
let dateProvider: DayjsDateProvider;
let createDoctorService: CreateDoctorService;
let createPatientService: CreatePatientService;
let createAppointmentService: CreateAppointmentService;
let listPatientAppointmentCommentService: ListPatientAppointmentCommentService;

let appointment: Appointment;
let doctor: Doctor;
let patient: Patient;
let currentDate: Date;
let currentYear: number;
let currentMonth: number;
let currentDay: number;

describe("List Patient Appointment Comments", () => {
  beforeEach(async () => {
    dateProvider = new DayjsDateProvider();

    doctorRepositoryTest = new DoctorRepositoryTest();
    createDoctorService = new CreateDoctorService(doctorRepositoryTest);

    patientRepositoryTest = new PatientRepositoryTest();
    createPatientService = new CreatePatientService(
      patientRepositoryTest,
      doctorRepositoryTest
    );

    appointmentRepositoryTest = new AppointmentRepositoryTest();
    createAppointmentService = new CreateAppointmentService(
      appointmentRepositoryTest,
      patientRepositoryTest,
      dateProvider
    );

    listPatientAppointmentCommentService =
      new ListPatientAppointmentCommentService(
        appointmentRepositoryTest,
        patientRepositoryTest
      );

    const newDoctor = await createDoctorService.execute({
      name: "Doctor Test",
      email: "doctor@test.com",
      password: "123456",
    });

    doctor = newDoctor;

    const newPatient = await createPatientService.execute({
      doctor_id: doctor.id,
      name: "Patient Test",
      phone: "(48) 9 8875-0125",
      email: "patient@test.com",
      birth_date: "1985-07-16",
      gender: "M",
      height: 1.67,
      weight: 70.4,
    });

    patient = newPatient;

    currentDate = new Date();
    currentYear = currentDate.getFullYear();
    currentMonth = currentDate.getMonth() + 1;
    currentDay = currentDate.getDate();

    const appointmentDate = new Date(
      `${currentYear + 1}-${currentMonth}-${currentDay} 09:00:00`
    );

    appointment = await createAppointmentService.execute({
      doctor_id: doctor.id,
      patient_id: patient.id,
      date: appointmentDate,
    });
  });

  it("must list patient appointment comments", async () => {
    const appointments = await listPatientAppointmentCommentService.execute({
      doctor_id: doctor.id,
      patient_id: patient.id,
    });

    expect(appointments).toEqual([appointment]);
    expect(appointments.length).toBe(1);
  });

  it("should not list patient appointment comments to a non-existent doctor", async () => {
    const appointments = await listPatientAppointmentCommentService.execute({
      doctor_id: uuid(),
      patient_id: patient.id,
    });

    expect(appointments.length).toBe(0);
  });

  it("should not list patient appointment comments to a non-existent patient", async () => {
    await expect(
      listPatientAppointmentCommentService.execute({
        doctor_id: doctor.id,
        patient_id: uuid(),
      })
    ).rejects.toEqual(new AppError("Patient not found"));
  });
});

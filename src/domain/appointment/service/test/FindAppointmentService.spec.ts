import { v4 as uuid } from "uuid";

import { AppError } from "@common/error/AppError";
import { DayjsDateProvider } from "@common/provider/DateProvider/implementation/DayjsDateProvider";
import { Appointment } from "@domain/appointment/entity/Appointment";
import { AppointmentRepositoryTest } from "@domain/appointment/repository/test/AppointmentRepositoryTest";
import { CreateAppointmentService } from "@domain/appointment/service/CreateAppointmentService";
import { FindAppointmentService } from "@domain/appointment/service/FindAppointmentService";
import { Doctor } from "@domain/doctor/entity/Doctor";
import { DoctorRepositoryTest } from "@domain/doctor/repository/test/DoctorRepositoryTest";
import { CreateDoctorService } from "@domain/doctor/service/CreateDoctorService";
import { Patient } from "@domain/patient/entity/Patient";
import { PatientRepositoryTest } from "@domain/patient/repository/test/PatientRepositoryTest";
import { CreatePatientService } from "@domain/patient/service/CreatePatientService";

let patientRepositoryTest: PatientRepositoryTest;
let doctorRepositoryTest: DoctorRepositoryTest;
let appointmentRepositoryTest: AppointmentRepositoryTest;
let dateProvider: DayjsDateProvider;
let createDoctorService: CreateDoctorService;
let createPatientService: CreatePatientService;
let createAppointmentService: CreateAppointmentService;
let findAppointmentService: FindAppointmentService;

let appointment: Appointment;
let doctor: Doctor;
let patient: Patient;
let currentDate: Date;
let currentYear: number;
let currentMonth: number;
let currentDay: number;

describe("Find Appointment", () => {
  beforeEach(async () => {
    dateProvider = new DayjsDateProvider();

    doctorRepositoryTest = new DoctorRepositoryTest();
    createDoctorService = new CreateDoctorService(doctorRepositoryTest);

    patientRepositoryTest = new PatientRepositoryTest();
    createPatientService = new CreatePatientService(
      patientRepositoryTest,
      doctorRepositoryTest
    );

    appointmentRepositoryTest = new AppointmentRepositoryTest();
    createAppointmentService = new CreateAppointmentService(
      appointmentRepositoryTest,
      patientRepositoryTest,
      dateProvider
    );

    findAppointmentService = new FindAppointmentService(
      appointmentRepositoryTest,
      doctorRepositoryTest,
      patientRepositoryTest
    );

    const newDoctor = await createDoctorService.execute({
      name: "Doctor Test",
      email: "doctor@test.com",
      password: "123456",
    });

    doctor = newDoctor;

    const newPatient = await createPatientService.execute({
      doctor_id: doctor.id,
      name: "Patient Test",
      phone: "(48) 9 8875-0125",
      email: "patient@test.com",
      birth_date: "1985-07-16",
      gender: "M",
      height: 1.67,
      weight: 70.4,
    });

    patient = newPatient;

    currentDate = new Date();
    currentYear = currentDate.getFullYear();
    currentMonth = currentDate.getMonth() + 1;
    currentDay = currentDate.getDate();

    const appointmentDate = new Date(
      `${currentYear + 1}-${currentMonth}-${currentDay} 09:00:00`
    );

    appointment = await createAppointmentService.execute({
      doctor_id: doctor.id,
      patient_id: patient.id,
      date: appointmentDate,
    });
  });

  it("must find appointments", async () => {
    const appointments = await findAppointmentService.execute({});

    expect(appointments).toEqual([appointment]);
    expect(appointments.length).toBe(1);
  });

  it("must find appointments to a doctor", async () => {
    const appointments = await findAppointmentService.execute({
      doctor_id: doctor.id,
    });

    expect(appointments).toEqual([appointment]);
    expect(appointments.length).toBe(1);
  });

  it("should not find appointments to a non-existent doctor", async () => {
    await expect(
      findAppointmentService.execute({
        doctor_id: uuid(),
      })
    ).rejects.toEqual(new AppError("Doctor not found"));
  });

  it("must find appointments to a patient", async () => {
    const appointments = await findAppointmentService.execute({
      patient_id: patient.id,
    });

    expect(appointments).toEqual([appointment]);
    expect(appointments.length).toBe(1);
  });

  it("should not find appointments to a non-existent patient", async () => {
    await expect(
      findAppointmentService.execute({
        patient_id: uuid(),
      })
    ).rejects.toEqual(new AppError("Patient not found"));
  });

  it("must find appointments to a date", async () => {
    const appointmentDate = new Date(
      `${currentYear + 1}-${currentMonth}-${currentDay} 09:00:00`
    );

    const appointments = await findAppointmentService.execute({
      date: dateProvider.convertToUTC(appointmentDate),
    });

    expect(appointments).toEqual([appointment]);
    expect(appointments.length).toBe(1);
  });
});

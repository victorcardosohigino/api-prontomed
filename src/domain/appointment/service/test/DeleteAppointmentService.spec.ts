import { v4 as uuid } from "uuid";

import { AppError } from "@common/error/AppError";
import { DayjsDateProvider } from "@common/provider/DateProvider/implementation/DayjsDateProvider";
import { Appointment } from "@domain/appointment/entity/Appointment";
import { AppointmentRepositoryTest } from "@domain/appointment/repository/test/AppointmentRepositoryTest";
import { CreateAppointmentService } from "@domain/appointment/service/CreateAppointmentService";
import { DeleteAppointmentService } from "@domain/appointment/service/DeleteAppointmentService";
import { Doctor } from "@domain/doctor/entity/Doctor";
import { DoctorRepositoryTest } from "@domain/doctor/repository/test/DoctorRepositoryTest";
import { CreateDoctorService } from "@domain/doctor/service/CreateDoctorService";
import { Patient } from "@domain/patient/entity/Patient";
import { PatientRepositoryTest } from "@domain/patient/repository/test/PatientRepositoryTest";
import { CreatePatientService } from "@domain/patient/service/CreatePatientService";

let patientRepositoryTest: PatientRepositoryTest;
let doctorRepositoryTest: DoctorRepositoryTest;
let appointmentRepositoryTest: AppointmentRepositoryTest;
let dateProvider: DayjsDateProvider;
let createDoctorService: CreateDoctorService;
let createPatientService: CreatePatientService;
let createAppointmentService: CreateAppointmentService;
let deleteAppointmentService: DeleteAppointmentService;

let appointment: Appointment;
let doctor: Doctor;
let patient: Patient;
let currentDate: Date;
let currentYear: number;
let currentMonth: number;
let currentDay: number;

describe("Delete Appointment", () => {
  beforeEach(async () => {
    dateProvider = new DayjsDateProvider();

    doctorRepositoryTest = new DoctorRepositoryTest();
    createDoctorService = new CreateDoctorService(doctorRepositoryTest);

    patientRepositoryTest = new PatientRepositoryTest();
    createPatientService = new CreatePatientService(
      patientRepositoryTest,
      doctorRepositoryTest
    );

    appointmentRepositoryTest = new AppointmentRepositoryTest();
    createAppointmentService = new CreateAppointmentService(
      appointmentRepositoryTest,
      patientRepositoryTest,
      dateProvider
    );

    deleteAppointmentService = new DeleteAppointmentService(
      appointmentRepositoryTest,
      doctorRepositoryTest
    );

    const newDoctor = await createDoctorService.execute({
      name: "Doctor Test",
      email: "doctor@test.com",
      password: "123456",
    });

    doctor = newDoctor;

    const newPatient = await createPatientService.execute({
      doctor_id: doctor.id,
      name: "Patient Test",
      phone: "(48) 9 8875-0125",
      email: "patient@test.com",
      birth_date: "1985-07-16",
      gender: "M",
      height: 1.67,
      weight: 70.4,
    });

    patient = newPatient;

    currentDate = new Date();
    currentYear = currentDate.getFullYear();
    currentMonth = currentDate.getMonth() + 1;
    currentDay = currentDate.getDate();

    const appointmentDate = new Date(
      `${currentYear + 1}-${currentMonth}-${currentDay} 09:00:00`
    );

    appointment = await createAppointmentService.execute({
      doctor_id: doctor.id,
      patient_id: patient.id,
      date: appointmentDate,
    });
  });

  it("must delete an appointment", async () => {
    expect(
      await deleteAppointmentService.execute({
        id: appointment.id,
        doctor_id: doctor.id,
      })
    ).not.toBeInstanceOf(AppError);
  });

  it("should not delete an non-existent appointment", async () => {
    await expect(
      deleteAppointmentService.execute({
        id: uuid(),
        doctor_id: doctor.id,
      })
    ).rejects.toEqual(new AppError("Appointment not found"));
  });

  it("should not delete an non-existent doctor", async () => {
    await expect(
      deleteAppointmentService.execute({
        id: appointment.id,
        doctor_id: uuid(),
      })
    ).rejects.toEqual(new AppError("Doctor not found"));
  });
});

import { inject, injectable } from "tsyringe";

import { AppError } from "@common/error/AppError";
import { IDateProvider } from "@common/provider/DateProvider/IDateProvider";
import appointmentConfiguration from "@configuration/appointment";
import { IEditAppointmentDTO } from "@domain/appointment/dto/IEditAppointmentDTO";
import { Appointment } from "@domain/appointment/entity/Appointment";
import { IAppointmentRepository } from "@domain/appointment/repository/IAppointmentRepository";
import { IPatientRepository } from "@domain/patient/repository/IPatientRepository";

@injectable()
class EditAppointmentService {
  constructor(
    @inject("AppointmentRepository")
    private appointmentRepository: IAppointmentRepository,
    @inject("PatientRepository")
    private patientRepository: IPatientRepository,
    @inject("DayjsDateProvider")
    private dateProvider: IDateProvider
  ) {}

  async execute({
    id,
    doctor_id,
    patient_id,
    date,
    comment,
  }: IEditAppointmentDTO): Promise<Appointment> {
    const appointment = await this.appointmentRepository.findById(id);

    if (!appointment) {
      throw new AppError("Appointment not exists");
    }

    const patient = await this.patientRepository.findById(patient_id);

    if (!patient) {
      throw new AppError("Patient not found");
    }

    const currentDate = this.dateProvider.currentDate();

    if (this.dateProvider.compareIfBefore(date, currentDate)) {
      throw new AppError(
        "The appointment date cannot be less than the current date"
      );
    }

    const appointmentSameDate =
      await this.appointmentRepository.findByDoctorAndDate({
        doctor_id,
        date,
      });

    if (appointmentSameDate && appointmentSameDate.id !== id) {
      throw new AppError("The apppointment date is not available");
    }

    const dateLess = this.dateProvider.subtractMinutesToDate(
      date,
      appointmentConfiguration.duration
    );
    const dateGreater = this.dateProvider.addMinutesToDate(
      date,
      appointmentConfiguration.duration
    );

    const appointmentSameHour =
      await this.appointmentRepository.findByDoctorAndBetweenDate({
        doctor_id,
        date_less: dateLess.toISOString(),
        date_greater: dateGreater.toISOString(),
        appointment_id: id,
      });

    if (appointmentSameHour) {
      throw new AppError(
        `The appointment must have a duration of ${appointmentConfiguration.duration} minutes`
      );
    }

    Object.assign(appointment, {
      doctor_id,
      patient_id,
      date,
      comment,
    });

    await this.appointmentRepository.save(appointment);

    return appointment;
  }
}

export { EditAppointmentService };

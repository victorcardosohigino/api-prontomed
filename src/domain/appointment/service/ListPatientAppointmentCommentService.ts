import { inject, injectable } from "tsyringe";

import { AppError } from "@common/error/AppError";
import { IListPatientAppointmentCommentDTO } from "@domain/appointment/dto/IListPatientAppointmentCommentDTO";
import { Appointment } from "@domain/appointment/entity/Appointment";
import { IAppointmentRepository } from "@domain/appointment/repository/IAppointmentRepository";
import { IPatientRepository } from "@domain/patient/repository/IPatientRepository";

@injectable()
class ListPatientAppointmentCommentService {
  constructor(
    @inject("AppointmentRepository")
    private appointmentRepository: IAppointmentRepository,
    @inject("PatientRepository")
    private patientRepository: IPatientRepository
  ) {}

  async execute({
    doctor_id,
    patient_id,
  }: IListPatientAppointmentCommentDTO): Promise<Appointment[]> {
    const patient = await this.patientRepository.findById(patient_id);

    if (!patient) {
      throw new AppError("Patient not found");
    }

    const appointments =
      await this.appointmentRepository.findAppointmentCommentsByPatient({
        doctor_id,
        patient_id,
      });

    return appointments;
  }
}

export { ListPatientAppointmentCommentService };

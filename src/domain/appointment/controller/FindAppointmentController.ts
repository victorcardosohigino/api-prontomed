import { Request, Response } from "express";
import { container } from "tsyringe";

import { FindAppointmentService } from "@domain/appointment/service/FindAppointmentService";

class FindAppointmentController {
  async handle(request: Request, response: Response): Promise<Response> {
    const doctor_id = request.doctor.doctor_id as string;
    const patient_id = request.query.patient_id as string;
    const date = request.query.date as string;

    const findAppointmentService = container.resolve(FindAppointmentService);

    const appointments = await findAppointmentService.execute({
      doctor_id,
      patient_id,
      date,
    });

    return response.json(appointments);
  }
}

export { FindAppointmentController };

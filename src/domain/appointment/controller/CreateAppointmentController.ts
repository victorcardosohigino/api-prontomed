import { Request, Response } from "express";
import { container } from "tsyringe";

import { CreateAppointmentService } from "@domain/appointment/service/CreateAppointmentService";

class CreateAppointmentController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { patient_id, date, comment } = request.body;
    const doctor_id = request.doctor.doctor_id as string;

    const createAppointmentService = container.resolve(
      CreateAppointmentService
    );

    const appointment = await createAppointmentService.execute({
      doctor_id,
      patient_id,
      date,
      comment,
    });

    return response.status(201).send(appointment);
  }
}

export { CreateAppointmentController };

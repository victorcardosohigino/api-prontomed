import { Request, Response } from "express";
import { container } from "tsyringe";

import { ListPatientAppointmentCommentService } from "@domain/appointment/service/ListPatientAppointmentCommentService";

class ListPatientAppointmentCommentController {
  async handle(request: Request, response: Response): Promise<Response> {
    const doctor_id = request.doctor.doctor_id as string;
    const patient_id = request.query.patient_id as string;

    const listPatientAppointmentCommentService = container.resolve(
      ListPatientAppointmentCommentService
    );

    const appointments = await listPatientAppointmentCommentService.execute({
      doctor_id,
      patient_id,
    });

    return response.json(appointments);
  }
}

export { ListPatientAppointmentCommentController };

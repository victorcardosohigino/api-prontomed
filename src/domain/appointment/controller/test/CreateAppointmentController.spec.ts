import { hash } from "bcrypt";
import request from "supertest";
import { Connection } from "typeorm";
import { v4 as uuid } from "uuid";

import createConnection from "@common/database/connection";
import { app } from "@common/http/app";

let connection: Connection;

describe("POST /appointment", () => {
  beforeAll(async () => {
    connection = await createConnection();
    await connection.runMigrations();

    const id = uuid();
    const id2 = uuid();
    const password = await hash("123456", 8);

    await connection.query(
      `INSERT INTO doctor(id, name, email, password, active, created_at, updated_at)
        values('${id}', 'Doctor Test', 'doctor@test.com', '${password}', true, 'now()', 'now()')`
    );

    await connection.query(
      `INSERT INTO doctor(id, name, email, password, active, created_at, updated_at)
        values('${id2}', 'Doctor Test 2', 'doctor2@test.com', '${password}', true, 'now()', 'now()')`
    );
  });

  afterAll(async () => {
    await connection.dropDatabase();
    await connection.close();
  });

  it("must create a new appointment ", async () => {
    const currentDate = new Date();
    const currentYear = currentDate.getFullYear();
    const currentMonth = currentDate.getMonth() + 1;
    const currentDay = currentDate.getDate();
    const appointmentDate = new Date(
      `${currentYear + 1}-${currentMonth}-${currentDay} 09:00:00`
    );

    const responseAuth = await request(app)
      .post("/authentication/session")
      .send({
        email: "doctor@test.com",
        password: "123456",
      });

    const { access_token } = responseAuth.body;

    const responsePatient = await request(app)
      .post("/patient")
      .send({
        name: "Patient Test",
        phone: "(48) 9 8875-0125",
        email: "patient@test.com",
        birth_date: "1985-07-16",
        gender: "M",
        height: 1.67,
        weight: 70.4,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    const { id } = responsePatient.body;

    const response = await request(app)
      .post("/appointment")
      .send({
        patient_id: id,
        date: appointmentDate,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    expect(response.status).toBe(201);
  });

  it("should not create an appointment on a past date", async () => {
    const currentDate = new Date();
    const currentYear = currentDate.getFullYear();
    const currentMonth = currentDate.getMonth() + 1;
    const currentDay = currentDate.getDate();
    const appointmentDate = new Date(
      `${currentYear - 1}-${currentMonth}-${currentDay} 09:00:00`
    );

    const responseAuth = await request(app)
      .post("/authentication/session")
      .send({
        email: "doctor@test.com",
        password: "123456",
      });

    const { access_token } = responseAuth.body;

    const responsePatient = await request(app)
      .post("/patient")
      .send({
        name: "Patient Test",
        phone: "(48) 9 8875-0125",
        email: "patient@test.com",
        birth_date: "1985-07-16",
        gender: "M",
        height: 1.67,
        weight: 70.4,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    const { id } = responsePatient.body;

    const response = await request(app)
      .post("/appointment")
      .send({
        patient_id: id,
        date: appointmentDate,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    expect(response.status).toBe(400);
  });

  it("should not create an appointment with the same date", async () => {
    const currentDate = new Date();
    const currentYear = currentDate.getFullYear();
    const currentMonth = currentDate.getMonth() + 1;
    const currentDay = currentDate.getDate();
    const appointmentDate = new Date(
      `${currentYear + 1}-${currentMonth}-${currentDay} 09:00:00`
    );

    const responseAuth = await request(app)
      .post("/authentication/session")
      .send({
        email: "doctor@test.com",
        password: "123456",
      });

    const { access_token } = responseAuth.body;

    const responsePatient = await request(app)
      .post("/patient")
      .send({
        name: "Patient Test",
        phone: "(48) 9 8875-0125",
        email: "patient@test.com",
        birth_date: "1985-07-16",
        gender: "M",
        height: 1.67,
        weight: 70.4,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    const { id } = responsePatient.body;

    await request(app)
      .post("/appointment")
      .send({
        patient_id: id,
        date: appointmentDate,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    const response = await request(app)
      .post("/appointment")
      .send({
        patient_id: id,
        date: appointmentDate,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    expect(response.status).toBe(400);
  });

  it("must create an appointment with the same date to different doctor", async () => {
    const currentDate = new Date();
    const currentYear = currentDate.getFullYear();
    const currentMonth = currentDate.getMonth() + 1;
    const currentDay = currentDate.getDate();
    const appointmentDate = new Date(
      `${currentYear + 1}-${currentMonth}-${currentDay} 09:00:00`
    );

    const responseAuth = await request(app)
      .post("/authentication/session")
      .send({
        email: "doctor@test.com",
        password: "123456",
      });

    const { access_token } = responseAuth.body;

    const responsePatient = await request(app)
      .post("/patient")
      .send({
        name: "Patient Test",
        phone: "(48) 9 8875-0125",
        email: "patient@test.com",
        birth_date: "1985-07-16",
        gender: "M",
        height: 1.67,
        weight: 70.4,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    const { id } = responsePatient.body;

    await request(app)
      .post("/appointment")
      .send({
        patient_id: id,
        date: appointmentDate,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    const responseAuthTwo = await request(app)
      .post("/authentication/session")
      .send({
        email: "doctor2@test.com",
        password: "123456",
      });

    const accessTokenTwo = responseAuthTwo.body.access_token;

    const responsePatientTwo = await request(app)
      .post("/patient")
      .send({
        name: "Patient Test 2",
        phone: "(48) 9 8875-0126",
        email: "patient2@test.com",
        birth_date: "1985-07-16",
        gender: "M",
        height: 1.67,
        weight: 70.4,
      })
      .set({
        Authorization: `Bearer ${accessTokenTwo}`,
      });

    const id2 = responsePatientTwo.body.id;

    const response = await request(app)
      .post("/appointment")
      .send({
        patient_id: id2,
        date: appointmentDate,
      })
      .set({
        Authorization: `Bearer ${accessTokenTwo}`,
      });

    expect(response.status).toBe(201);
  });

  it("should not create an appointment with the same date interval", async () => {
    const currentDate = new Date();
    const currentYear = currentDate.getFullYear();
    const currentMonth = currentDate.getMonth() + 1;
    const currentDay = currentDate.getDate();
    const appointmentDate = new Date(
      `${currentYear + 1}-${currentMonth}-${currentDay} 09:00:00`
    );
    const appointmentDateTwo = new Date(
      `${currentYear + 1}-${currentMonth}-${currentDay} 09:30:00`
    );

    const responseAuth = await request(app)
      .post("/authentication/session")
      .send({
        email: "doctor@test.com",
        password: "123456",
      });

    const { access_token } = responseAuth.body;

    const responsePatient = await request(app)
      .post("/patient")
      .send({
        name: "Patient Test",
        phone: "(48) 9 8875-0125",
        email: "patient@test.com",
        birth_date: "1985-07-16",
        gender: "M",
        height: 1.67,
        weight: 70.4,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    const { id } = responsePatient.body;

    await request(app)
      .post("/appointment")
      .send({
        patient_id: id,
        date: appointmentDate,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    const response = await request(app)
      .post("/appointment")
      .send({
        patient_id: id,
        date: appointmentDateTwo,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    expect(response.status).toBe(400);
  });

  it("should not create an appointment to a non-existent patient", async () => {
    const currentDate = new Date();
    const currentYear = currentDate.getFullYear();
    const currentMonth = currentDate.getMonth() + 1;
    const currentDay = currentDate.getDate();
    const appointmentDate = new Date(
      `${currentYear + 1}-${currentMonth}-${currentDay} 09:00:00`
    );

    const responseAuth = await request(app)
      .post("/authentication/session")
      .send({
        email: "doctor@test.com",
        password: "123456",
      });

    const { access_token } = responseAuth.body;

    const response = await request(app)
      .post("/appointment")
      .send({
        patient_id: uuid(),
        date: appointmentDate,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    expect(response.status).toBe(400);
  });
});

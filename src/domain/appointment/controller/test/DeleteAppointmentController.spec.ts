import { hash } from "bcrypt";
import request from "supertest";
import { Connection } from "typeorm";
import { v4 as uuid } from "uuid";

import createConnection from "@common/database/connection";
import { app } from "@common/http/app";

let connection: Connection;
let doctorId: string;

describe("DELETE /appointment/:id", () => {
  beforeAll(async () => {
    connection = await createConnection();
    await connection.runMigrations();

    doctorId = uuid();
    const password = await hash("123456", 8);

    await connection.query(
      `INSERT INTO doctor(id, name, email, password, active, created_at, updated_at)
        values('${doctorId}', 'Doctor Test', 'doctor@test.com', '${password}', true, 'now()', 'now()')`
    );
  });

  afterAll(async () => {
    await connection.dropDatabase();
    await connection.close();
  });

  it("must delete a appointment ", async () => {
    const currentDate = new Date();
    const currentYear = currentDate.getFullYear();
    const currentMonth = currentDate.getMonth() + 1;
    const currentDay = currentDate.getDate();
    const appointmentDate = new Date(
      `${currentYear + 1}-${currentMonth}-${currentDay} 09:00:00`
    );

    const responseAuth = await request(app)
      .post("/authentication/session")
      .send({
        email: "doctor@test.com",
        password: "123456",
      });

    const { access_token } = responseAuth.body;

    const responsePatient = await request(app)
      .post("/patient")
      .send({
        name: "Patient Test",
        phone: "(48) 9 8875-0125",
        email: "patient@test.com",
        birth_date: "1985-07-16",
        gender: "M",
        height: 1.67,
        weight: 70.4,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    const { id } = responsePatient.body;

    const responseAppointment = await request(app)
      .post("/appointment")
      .send({
        patient_id: id,
        date: appointmentDate,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    const response = await request(app)
      .delete(`/appointment/${responseAppointment.body.id}`)
      .send({
        doctor_id: doctorId,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    expect(response.status).toBe(204);
  });

  it("should not delete an appointment", async () => {
    const currentDate = new Date();
    const currentYear = currentDate.getFullYear();
    const currentMonth = currentDate.getMonth() + 1;
    const currentDay = currentDate.getDate();
    const appointmentDate = new Date(
      `${currentYear + 1}-${currentMonth}-${currentDay} 09:00:00`
    );

    const responseAuth = await request(app)
      .post("/authentication/session")
      .send({
        email: "doctor@test.com",
        password: "123456",
      });

    const { access_token } = responseAuth.body;

    const responsePatient = await request(app)
      .post("/patient")
      .send({
        name: "Patient Test",
        phone: "(48) 9 8875-0125",
        email: "patient@test.com",
        birth_date: "1985-07-16",
        gender: "M",
        height: 1.67,
        weight: 70.4,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    const { id } = responsePatient.body;

    await request(app)
      .post("/appointment")
      .send({
        patient_id: id,
        date: appointmentDate,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    const response = await request(app)
      .delete(`/appointment/${uuid()}`)
      .send({
        doctor_id: doctorId,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    expect(response.status).toBe(400);
  });

  it("should not delete an appointment with a non-existent doctor", async () => {
    const currentDate = new Date();
    const currentYear = currentDate.getFullYear();
    const currentMonth = currentDate.getMonth() + 1;
    const currentDay = currentDate.getDate();
    const appointmentDate = new Date(
      `${currentYear + 1}-${currentMonth}-${currentDay} 09:00:00`
    );

    const responseAuth = await request(app)
      .post("/authentication/session")
      .send({
        email: "doctor@test.com",
        password: "123456",
      });

    const { access_token } = responseAuth.body;

    const responsePatient = await request(app)
      .post("/patient")
      .send({
        name: "Patient Test",
        phone: "(48) 9 8875-0125",
        email: "patient@test.com",
        birth_date: "1985-07-16",
        gender: "M",
        height: 1.67,
        weight: 70.4,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    const { id } = responsePatient.body;

    const responseAppointment = await request(app)
      .post("/appointment")
      .send({
        patient_id: id,
        date: appointmentDate,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    const response = await request(app)
      .delete(`/appointment/${responseAppointment.body.id}`)
      .send({
        doctor_id: uuid(),
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    expect(response.status).toBe(400);
  });
});

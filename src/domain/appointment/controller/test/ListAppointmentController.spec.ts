import { hash } from "bcrypt";
import request from "supertest";
import { Connection } from "typeorm";
import { v4 as uuid } from "uuid";

import createConnection from "@common/database/connection";
import { app } from "@common/http/app";

let connection: Connection;
let doctorId: string;

describe("GET /appointment/list", () => {
  beforeAll(async () => {
    connection = await createConnection();
    await connection.runMigrations();

    doctorId = uuid();
    const password = await hash("123456", 8);

    await connection.query(
      `INSERT INTO doctor(id, name, email, password, active, created_at, updated_at)
        values('${doctorId}', 'Doctor Test', 'doctor@test.com', '${password}', true, 'now()', 'now()')`
    );
  });

  afterAll(async () => {
    await connection.dropDatabase();
    await connection.close();
  });

  it("must list appointments", async () => {
    const currentDate = new Date();
    const currentYear = currentDate.getFullYear();
    const currentMonth = currentDate.getMonth() + 1;
    const currentDay = currentDate.getDate();
    const appointmentDate = new Date(
      `${currentYear + 1}-${currentMonth}-${currentDay} 09:00:00`
    );

    const responseAuth = await request(app)
      .post("/authentication/session")
      .send({
        email: "doctor@test.com",
        password: "123456",
      });

    const { access_token } = responseAuth.body;

    const responsePatient = await request(app)
      .post("/patient")
      .send({
        name: "Patient Test",
        phone: "(48) 9 8875-0125",
        email: "patient@test.com",
        birth_date: "1985-07-16",
        gender: "M",
        height: 1.67,
        weight: 70.4,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    const { id } = responsePatient.body;

    await request(app)
      .post("/appointment")
      .send({
        patient_id: id,
        date: appointmentDate,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    const response = await request(app)
      .get("/appointment/list")
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    expect(response.status).toBe(200);
  });

  it("must list appointments to a patient", async () => {
    const currentDate = new Date();
    const currentYear = currentDate.getFullYear();
    const currentMonth = currentDate.getMonth() + 1;
    const currentDay = currentDate.getDate();
    const appointmentDate = new Date(
      `${currentYear + 1}-${currentMonth}-${currentDay} 09:00:00`
    );

    const responseAuth = await request(app)
      .post("/authentication/session")
      .send({
        email: "doctor@test.com",
        password: "123456",
      });

    const { access_token } = responseAuth.body;

    const responsePatient = await request(app)
      .post("/patient")
      .send({
        name: "Patient Test",
        phone: "(48) 9 8875-0125",
        email: "patient@test.com",
        birth_date: "1985-07-16",
        gender: "M",
        height: 1.67,
        weight: 70.4,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    const { id } = responsePatient.body;

    await request(app)
      .post("/appointment")
      .send({
        patient_id: id,
        date: appointmentDate,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    const response = await request(app)
      .get("/appointment/list")
      .query({
        patient_id: id,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    expect(response.status).toBe(200);
  });

  it("must list appointments with offset and limit", async () => {
    const currentDate = new Date();
    const currentYear = currentDate.getFullYear();
    const currentMonth = currentDate.getMonth() + 1;
    const currentDay = currentDate.getDate();
    const appointmentDate = new Date(
      `${currentYear + 1}-${currentMonth}-${currentDay} 09:00:00`
    );

    const responseAuth = await request(app)
      .post("/authentication/session")
      .send({
        email: "doctor@test.com",
        password: "123456",
      });

    const { access_token } = responseAuth.body;

    const responsePatient = await request(app)
      .post("/patient")
      .send({
        name: "Patient Test",
        phone: "(48) 9 8875-0125",
        email: "patient@test.com",
        birth_date: "1985-07-16",
        gender: "M",
        height: 1.67,
        weight: 70.4,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    const { id } = responsePatient.body;

    await request(app)
      .post("/appointment")
      .send({
        patient_id: id,
        date: appointmentDate,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    const response = await request(app)
      .get("/appointment/list")
      .query({
        offset: 0,
        limit: 1,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    expect(response.status).toBe(200);
  });
});

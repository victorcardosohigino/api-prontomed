import { Request, Response } from "express";
import { container } from "tsyringe";

import { ListAppointmentService } from "@domain/appointment/service/ListAppointmentService";

class ListAppointmentController {
  async handle(request: Request, response: Response): Promise<Response> {
    const doctor_id = request.doctor.doctor_id as string;
    const patient_id = request.query.patient_id as string;
    const offset = request.query.offset as string;
    const limit = request.query.limit as string;

    const listAppointmentService = container.resolve(ListAppointmentService);

    const appointments = await listAppointmentService.execute({
      doctor_id,
      patient_id,
      offset: Number(offset),
      limit: Number(limit),
    });

    return response.json(appointments);
  }
}

export { ListAppointmentController };

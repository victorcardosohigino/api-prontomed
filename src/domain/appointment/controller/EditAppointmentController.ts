import { Request, Response } from "express";
import { container } from "tsyringe";

import { EditAppointmentService } from "@domain/appointment/service/EditAppointmentService";

class EditAppointmentController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { id } = request.params;
    const { patient_id, date, comment } = request.body;
    const doctor_id = request.doctor.doctor_id as string;

    const editAppointmentService = container.resolve(EditAppointmentService);

    await editAppointmentService.execute({
      id,
      doctor_id,
      patient_id,
      date,
      comment,
    });

    return response.status(204).send();
  }
}

export { EditAppointmentController };

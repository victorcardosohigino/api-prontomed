import { Request, Response } from "express";
import { container } from "tsyringe";

import { DeleteAppointmentService } from "@domain/appointment/service/DeleteAppointmentService";

class DeleteAppointmentController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { id } = request.params;
    const doctor_id = request.doctor.doctor_id as string;

    const deleteAppointmentService = container.resolve(
      DeleteAppointmentService
    );

    await deleteAppointmentService.execute({ id, doctor_id });

    return response.status(204).send();
  }
}

export { DeleteAppointmentController };

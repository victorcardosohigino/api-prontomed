interface IFindByDoctorAndDateDTO {
  doctor_id: string;
  date: Date;
}

export { IFindByDoctorAndDateDTO };

interface IFindByIdAndDoctorAppointmentDTO {
  id: string;
  doctor_id: string;
}

export { IFindByIdAndDoctorAppointmentDTO };

interface IFindByDoctorAndPatientDTO {
  doctor_id: string;
  patient_id: Date;
}

export { IFindByDoctorAndPatientDTO };

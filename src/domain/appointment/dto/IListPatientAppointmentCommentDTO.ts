interface IListPatientAppointmentCommentDTO {
  doctor_id: string;
  patient_id: string;
}

export { IListPatientAppointmentCommentDTO };

interface IFindByDoctorIdOrPatientIdOrDateDTO {
  doctor_id?: string;
  patient_id?: string;
  date?: string;
}

export { IFindByDoctorIdOrPatientIdOrDateDTO };

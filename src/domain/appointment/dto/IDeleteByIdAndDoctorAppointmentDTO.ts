interface IDeleteByIdAndDoctorAppointmentDTO {
  id: string;
  doctor_id: string;
}

export { IDeleteByIdAndDoctorAppointmentDTO };

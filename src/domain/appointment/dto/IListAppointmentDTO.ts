interface IListAppointmentDTO {
  doctor_id: string;
  patient_id?: string;
  offset?: number;
  limit?: number;
}

export { IListAppointmentDTO };

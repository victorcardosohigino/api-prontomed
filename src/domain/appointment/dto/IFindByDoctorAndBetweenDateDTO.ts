interface IFindByDoctorAndBetweenDateDTO {
  doctor_id: string;
  date_less: string;
  date_greater: string;
  appointment_id?: string;
}

export { IFindByDoctorAndBetweenDateDTO };

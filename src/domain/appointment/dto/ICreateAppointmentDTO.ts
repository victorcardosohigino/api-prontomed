interface ICreateAppointmentDTO {
  doctor_id: string;
  patient_id: string;
  date: Date;
  comment?: string;
}

export { ICreateAppointmentDTO };

import { hash } from "bcrypt";
import request from "supertest";
import { Connection } from "typeorm";
import { v4 as uuid } from "uuid";

import createConnection from "@common/database/connection";
import { app } from "@common/http/app";

let connection: Connection;

describe("POST /authentication/session", () => {
  beforeAll(async () => {
    connection = await createConnection();
    await connection.runMigrations();

    const id = uuid();
    const password = await hash("123456", 8);

    await connection.query(
      `INSERT INTO doctor(id, name, email, password, active, created_at, updated_at)
        values('${id}', 'Doctor Test', 'doctor@test.com', '${password}', true, 'now()', 'now()')`
    );
  });

  afterAll(async () => {
    await connection.dropDatabase();
    await connection.close();
  });

  it("must authenticate a doctor", async () => {
    const response = await request(app).post("/authentication/session").send({
      email: "doctor@test.com",
      password: "123456",
    });

    expect(response.status).toBe(200);
    expect(response.body).toHaveProperty("doctor");
    expect(response.body).toHaveProperty("access_token");
    expect(response.body).toHaveProperty("refresh_token");
    expect(response.body.doctor.email).toEqual("doctor@test.com");
  });

  it("should not be able to authenticate a doctor with incorrect email", async () => {
    const response = await request(app).post("/authentication/session").send({
      email: "wrongdoctor@test.com",
      password: "123456",
    });

    expect(response.status).toBe(400);
    expect(response.body).toHaveProperty("message");
    expect(response.body.message).toEqual("Email or password incorrect!");
  });

  it("should not be able to authenticate a doctor with incorrect password", async () => {
    const response = await request(app).post("/authentication/session").send({
      email: "doctor@test.com",
      password: "wrongpassword",
    });

    expect(response.status).toBe(400);
    expect(response.body).toHaveProperty("message");
    expect(response.body.message).toEqual("Email or password incorrect!");
  });
});

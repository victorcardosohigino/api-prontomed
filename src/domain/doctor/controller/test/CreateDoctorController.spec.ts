import request from "supertest";
import { Connection } from "typeorm";

import createConnection from "@common/database/connection";
import { app } from "@common/http/app";

let connection: Connection;

describe("POST /doctor", () => {
  beforeAll(async () => {
    connection = await createConnection();
    await connection.runMigrations();
  });

  afterAll(async () => {
    await connection.dropDatabase();
    await connection.close();
  });

  it("must create a new doctor ", async () => {
    const response = await request(app).post("/doctor").send({
      name: "Doctor Test",
      email: "doctor@test.com",
      password: "123456",
    });

    expect(response.status).toBe(201);
  });

  it("should not create a new doctor with the same email", async () => {
    const response = await request(app).post("/doctor").send({
      name: "Doctor Test",
      email: "doctor@test.com",
      password: "123456",
    });

    expect(response.status).toBe(400);
  });
});

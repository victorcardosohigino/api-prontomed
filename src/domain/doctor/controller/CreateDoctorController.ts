import { Request, Response } from "express";
import { container } from "tsyringe";

import { CreateDoctorService } from "@domain/doctor/service/CreateDoctorService";

class CreateDoctorController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { name, email, password } = request.body;

    const createDoctorService = container.resolve(CreateDoctorService);

    await createDoctorService.execute({
      name,
      email,
      password,
    });

    return response.status(201).send();
  }
}

export { CreateDoctorController };

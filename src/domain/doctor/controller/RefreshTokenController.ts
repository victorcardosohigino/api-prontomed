import { Request, Response } from "express";
import { container } from "tsyringe";

import { RefreshTokenService } from "@domain/doctor/service/RefreshTokenService";

class RefreshTokenController {
  async handle(request: Request, response: Response): Promise<Response> {
    const refreshToken =
      request.body.refresh_token ||
      request.headers["x-access-token"] ||
      request.query.refresh_token;

    const refreshTokenService = container.resolve(RefreshTokenService);

    const accessTokenAndRefreshToken = await refreshTokenService.execute(
      refreshToken
    );

    return response.json(accessTokenAndRefreshToken);
  }
}

export { RefreshTokenController };

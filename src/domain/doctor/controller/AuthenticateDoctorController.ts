import { Request, Response } from "express";
import { container } from "tsyringe";

import { AuthenticateDoctorService } from "@domain/doctor/service/AuthenticateDoctorService";

class AuthenticateDoctorController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { email, password } = request.body;

    const authenticateDoctorService = container.resolve(
      AuthenticateDoctorService
    );

    const token = await authenticateDoctorService.execute({
      email,
      password,
    });

    return response.json(token);
  }
}

export { AuthenticateDoctorController };

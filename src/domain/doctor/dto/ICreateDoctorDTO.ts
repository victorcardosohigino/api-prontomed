interface ICreateDoctorDTO {
  name: string;
  email: string;
  password: string;
}

export { ICreateDoctorDTO };

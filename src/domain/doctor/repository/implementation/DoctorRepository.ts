import { getRepository, Repository } from "typeorm";

import { ICreateDoctorDTO } from "@domain/doctor/dto/ICreateDoctorDTO";
import { Doctor } from "@domain/doctor/entity/Doctor";
import { IDoctorRepository } from "@domain/doctor/repository/IDoctorRepository";

class DoctorRepository implements IDoctorRepository {
  private repository: Repository<Doctor>;

  constructor() {
    this.repository = getRepository(Doctor);
  }

  async create({ name, email, password }: ICreateDoctorDTO): Promise<Doctor> {
    const doctor = this.repository.create({
      name,
      email,
      password,
    });

    await this.repository.save(doctor);

    return doctor;
  }

  async findByEmail(email: string): Promise<Doctor> {
    const doctor = await this.repository.findOne({ email, active: true });
    return doctor;
  }

  async findById(id: string): Promise<Doctor> {
    const doctor = await this.repository.findOne({
      id,
      active: true,
    });
    return doctor;
  }
}

export { DoctorRepository };

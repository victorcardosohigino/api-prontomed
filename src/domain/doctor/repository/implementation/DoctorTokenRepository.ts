import { getRepository, Repository } from "typeorm";

import { ICreateDoctorTokenDTO } from "@domain/doctor/dto/ICreateDoctorTokenDTO";
import { IFindByDoctorDTO } from "@domain/doctor/dto/IFindByDoctorDTO";
import { DoctorToken } from "@domain/doctor/entity/DoctorToken";
import { IDoctorTokenRepository } from "@domain/doctor/repository/IDoctorTokenRepository";

class DoctorTokenRepository implements IDoctorTokenRepository {
  private repository: Repository<DoctorToken>;

  constructor() {
    this.repository = getRepository(DoctorToken);
  }

  async create({
    doctor_id,
    expires_date,
    refresh_token,
  }: ICreateDoctorTokenDTO): Promise<DoctorToken> {
    const doctorToken = this.repository.create({
      doctor_id,
      expires_date,
      refresh_token,
    });

    await this.repository.save(doctorToken);

    return doctorToken;
  }

  async findByDoctorIdAndRefreshToken({
    doctor_id,
    refresh_token,
  }: IFindByDoctorDTO): Promise<DoctorToken> {
    const doctorToken = await this.repository.findOne({
      doctor_id,
      refresh_token,
    });

    return doctorToken;
  }

  async deleteById(id: string): Promise<void> {
    await this.repository.delete(id);
  }
}

export { DoctorTokenRepository };

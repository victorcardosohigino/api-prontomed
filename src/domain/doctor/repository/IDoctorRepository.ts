import { ICreateDoctorDTO } from "@domain/doctor/dto/ICreateDoctorDTO";
import { Doctor } from "@domain/doctor/entity/Doctor";

interface IDoctorRepository {
  create(data: ICreateDoctorDTO): Promise<Doctor>;
  findByEmail(email: string): Promise<Doctor>;
  findById(id: string): Promise<Doctor>;
}

export { IDoctorRepository };

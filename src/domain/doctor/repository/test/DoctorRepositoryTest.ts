import { ICreateDoctorDTO } from "@domain/doctor/dto/ICreateDoctorDTO";
import { Doctor } from "@domain/doctor/entity/Doctor";
import { IDoctorRepository } from "@domain/doctor/repository/IDoctorRepository";

class DoctorRepositoryTest implements IDoctorRepository {
  doctors: Doctor[] = [];

  async create({ name, email, password }: ICreateDoctorDTO): Promise<Doctor> {
    const doctor = new Doctor();

    Object.assign(doctor, {
      name,
      email,
      password,
    });

    this.doctors.push(doctor);

    return doctor;
  }

  async findByEmail(email: string): Promise<Doctor> {
    const doctor = this.doctors.find((doctor) => doctor.email === email);

    return doctor;
  }

  async findById(id: string): Promise<Doctor> {
    const doctor = this.doctors.find((doctor) => doctor.id === id);

    return doctor;
  }
}

export { DoctorRepositoryTest };

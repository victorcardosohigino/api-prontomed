import { ICreateDoctorTokenDTO } from "@domain/doctor/dto/ICreateDoctorTokenDTO";
import { IFindByDoctorDTO } from "@domain/doctor/dto/IFindByDoctorDTO";
import { DoctorToken } from "@domain/doctor/entity/DoctorToken";
import { IDoctorTokenRepository } from "@domain/doctor/repository/IDoctorTokenRepository";

class DoctorTokenRepositoryTest implements IDoctorTokenRepository {
  doctorsTokens: DoctorToken[] = [];

  async create({
    doctor_id,
    expires_date,
    refresh_token,
  }: ICreateDoctorTokenDTO): Promise<DoctorToken> {
    const doctorToken = new DoctorToken();

    Object.assign(doctorToken, {
      doctor_id,
      expires_date,
      refresh_token,
    });

    this.doctorsTokens.push(doctorToken);

    return doctorToken;
  }

  async findByDoctorIdAndRefreshToken({
    doctor_id,
    refresh_token,
  }: IFindByDoctorDTO): Promise<DoctorToken> {
    const doctorToken = this.doctorsTokens.find(
      (token) =>
        token.doctor_id === doctor_id && token.refresh_token === refresh_token
    );

    return doctorToken;
  }

  async deleteById(id: string): Promise<void> {
    const token = this.doctorsTokens.find((token) => token.id === id);

    this.doctorsTokens.splice(this.doctorsTokens.indexOf(token));
  }
}

export { DoctorTokenRepositoryTest };

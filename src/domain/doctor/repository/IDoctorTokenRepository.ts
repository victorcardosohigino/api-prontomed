import { ICreateDoctorTokenDTO } from "@domain/doctor/dto/ICreateDoctorTokenDTO";
import { IFindByDoctorDTO } from "@domain/doctor/dto/IFindByDoctorDTO";
import { DoctorToken } from "@domain/doctor/entity/DoctorToken";

interface IDoctorTokenRepository {
  create(data: ICreateDoctorTokenDTO): Promise<DoctorToken>;
  findByDoctorIdAndRefreshToken(data: IFindByDoctorDTO): Promise<DoctorToken>;
  deleteById(id: string): Promise<void>;
}

export { IDoctorTokenRepository };

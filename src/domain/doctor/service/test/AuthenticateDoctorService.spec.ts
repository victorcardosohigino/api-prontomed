import { AppError } from "@common/error/AppError";
import { DayjsDateProvider } from "@common/provider/DateProvider/implementation/DayjsDateProvider";
import { ICreateDoctorDTO } from "@domain/doctor/dto/ICreateDoctorDTO";
import { DoctorRepositoryTest } from "@domain/doctor/repository/test/DoctorRepositoryTest";
import { DoctorTokenRepositoryTest } from "@domain/doctor/repository/test/DoctorTokenRepositoryTest";
import { AuthenticateDoctorService } from "@domain/doctor/service/AuthenticateDoctorService";
import { CreateDoctorService } from "@domain/doctor/service/CreateDoctorService";

let doctorRepositoryTest: DoctorRepositoryTest;
let doctorTokenRepositoryTest: DoctorTokenRepositoryTest;
let dateProvider: DayjsDateProvider;

let createDoctorService: CreateDoctorService;
let authenticateDoctorService: AuthenticateDoctorService;

describe("Authenticate Doctor", () => {
  beforeEach(() => {
    doctorRepositoryTest = new DoctorRepositoryTest();
    doctorTokenRepositoryTest = new DoctorTokenRepositoryTest();
    dateProvider = new DayjsDateProvider();

    createDoctorService = new CreateDoctorService(doctorRepositoryTest);
    authenticateDoctorService = new AuthenticateDoctorService(
      doctorRepositoryTest,
      doctorTokenRepositoryTest,
      dateProvider
    );
  });

  it("must authenticate a doctor", async () => {
    const doctor: ICreateDoctorDTO = {
      name: "Doctor Test",
      email: "doctor@test.com",
      password: "123456",
    };

    await createDoctorService.execute(doctor);

    const authentication = await authenticateDoctorService.execute({
      email: doctor.email,
      password: doctor.password,
    });

    expect(authentication).toHaveProperty("access_token");
    expect(authentication).toHaveProperty("refresh_token");
    expect(authentication.doctor.name).toEqual(doctor.name);
    expect(authentication.doctor.email).toEqual(doctor.email);
  });

  it("should not authenticate a non-existent doctor", async () => {
    await expect(
      authenticateDoctorService.execute({
        email: "false@test.com",
        password: "123456",
      })
    ).rejects.toEqual(new AppError("Email or password incorrect!"));
  });

  it("should not authenticate with incorrect password", async () => {
    const doctor: ICreateDoctorDTO = {
      name: "Doctor Test",
      email: "doctor@test.com",
      password: "123456",
    };

    await createDoctorService.execute(doctor);

    await expect(
      authenticateDoctorService.execute({
        email: doctor.email,
        password: "test123",
      })
    ).rejects.toEqual(new AppError("Email or password incorrect!"));
  });
});

import { AppError } from "@common/error/AppError";
import { Doctor } from "@domain/doctor/entity/Doctor";
import { DoctorRepositoryTest } from "@domain/doctor/repository/test/DoctorRepositoryTest";
import { CreateDoctorService } from "@domain/doctor/service/CreateDoctorService";

let doctorRepositoryTest: DoctorRepositoryTest;
let createDoctorService: CreateDoctorService;

describe("Create Doctor", () => {
  beforeEach(() => {
    doctorRepositoryTest = new DoctorRepositoryTest();
    createDoctorService = new CreateDoctorService(doctorRepositoryTest);
  });

  it("must create a new doctor", async () => {
    const doctor = await createDoctorService.execute({
      name: "Doctor Test",
      email: "doctor@test.com",
      password: "123456",
    });

    expect(doctor).toBeInstanceOf(Doctor);
    expect(doctor).toHaveProperty("id");
  });

  it("should not create a new doctor with the same email", async () => {
    await createDoctorService.execute({
      name: "Doctor Test",
      email: "doctor@test.com",
      password: "123456",
    });

    await expect(
      createDoctorService.execute({
        name: "Doctor Test",
        email: "doctor@test.com",
        password: "123456",
      })
    ).rejects.toEqual(new AppError("Doctor already exists"));
  });
});

import { JsonWebTokenError, sign } from "jsonwebtoken";

import { AppError } from "@common/error/AppError";
import { DayjsDateProvider } from "@common/provider/DateProvider/implementation/DayjsDateProvider";
import auth from "@configuration/auth";
import { ICreateDoctorDTO } from "@domain/doctor/dto/ICreateDoctorDTO";
import { DoctorRepositoryTest } from "@domain/doctor/repository/test/DoctorRepositoryTest";
import { DoctorTokenRepositoryTest } from "@domain/doctor/repository/test/DoctorTokenRepositoryTest";
import { AuthenticateDoctorService } from "@domain/doctor/service/AuthenticateDoctorService";
import { CreateDoctorService } from "@domain/doctor/service/CreateDoctorService";
import { RefreshTokenService } from "@domain/doctor/service/RefreshTokenService";

let doctorRepositoryTest: DoctorRepositoryTest;
let doctorTokenRepositoryTest: DoctorTokenRepositoryTest;
let dateProvider: DayjsDateProvider;

let createDoctorService: CreateDoctorService;
let authenticateDoctorService: AuthenticateDoctorService;
let refreshTokenService: RefreshTokenService;

describe("Refresh Token", () => {
  beforeEach(() => {
    doctorRepositoryTest = new DoctorRepositoryTest();
    doctorTokenRepositoryTest = new DoctorTokenRepositoryTest();
    dateProvider = new DayjsDateProvider();

    createDoctorService = new CreateDoctorService(doctorRepositoryTest);
    authenticateDoctorService = new AuthenticateDoctorService(
      doctorRepositoryTest,
      doctorTokenRepositoryTest,
      dateProvider
    );

    refreshTokenService = new RefreshTokenService(
      doctorTokenRepositoryTest,
      dateProvider
    );
  });

  it("must generate a new token", async () => {
    const doctor: ICreateDoctorDTO = {
      name: "Doctor Test",
      email: "doctor@test.com",
      password: "123456",
    };

    await createDoctorService.execute(doctor);

    const authentication = await authenticateDoctorService.execute({
      email: doctor.email,
      password: doctor.password,
    });

    const refreshToken = await refreshTokenService.execute(
      authentication.refresh_token
    );

    expect(refreshToken).toHaveProperty("access_token");
    expect(refreshToken).toHaveProperty("refresh_token");
  });

  it("should not generate a refresh token for a non-existent token", async () => {
    const invalidRefreshToken = sign(
      { email: "wrongdoctor@test.com" },
      auth.secret_refresh_token
    );

    await expect(
      refreshTokenService.execute(invalidRefreshToken)
    ).rejects.toEqual(new AppError("Refresh Token does not exists!"));
  });

  it("should not generate a refresh token for a invalid signature", async () => {
    const invalidRefreshToken = sign(
      { email: "wrongdoctor@test.com" },
      "invalidSignature"
    );

    await expect(
      refreshTokenService.execute(invalidRefreshToken)
    ).rejects.toBeInstanceOf(JsonWebTokenError);
  });

  it("should not generate a refresh token for a invalid jwt", async () => {
    const invalidRefreshToken = "invalidRefreshToken";

    await expect(
      refreshTokenService.execute(invalidRefreshToken)
    ).rejects.toBeInstanceOf(JsonWebTokenError);
  });
});

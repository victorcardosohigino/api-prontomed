import { compare } from "bcrypt";
import { sign } from "jsonwebtoken";
import { inject, injectable } from "tsyringe";

import { AppError } from "@common/error/AppError";
import { IDateProvider } from "@common/provider/DateProvider/IDateProvider";
import auth from "@configuration/auth";
import { IDoctorRepository } from "@domain/doctor/repository/IDoctorRepository";
import { IDoctorTokenRepository } from "@domain/doctor/repository/IDoctorTokenRepository";

interface IRequest {
  email: string;
  password: string;
}

interface IResponse {
  doctor: {
    name: string;
    email: string;
  };
  access_token: string;
  refresh_token: string;
}

@injectable()
class AuthenticateDoctorService {
  constructor(
    @inject("DoctorRepository")
    private doctorRepository: IDoctorRepository,
    @inject("DoctorTokenRepository")
    private doctorTokenRepository: IDoctorTokenRepository,
    @inject("DayjsDateProvider")
    private dateProvider: IDateProvider
  ) {}

  async execute({ email, password }: IRequest): Promise<IResponse> {
    const doctor = await this.doctorRepository.findByEmail(email);

    const {
      expires_in_token,
      secret_refresh_token,
      secret_token,
      expires_in_refresh_token,
      expires_refresh_token_days,
    } = auth;

    if (!doctor) {
      throw new AppError("Email or password incorrect!");
    }

    const passwordMatch = await compare(password, doctor.password);

    if (!passwordMatch) {
      throw new AppError("Email or password incorrect!");
    }

    const accessToken = sign({}, secret_token, {
      subject: doctor.id,
      expiresIn: expires_in_token,
    });

    const refreshToken = sign({ email }, secret_refresh_token, {
      subject: doctor.id,
      expiresIn: expires_in_refresh_token,
    });

    const refreshTokenExpiresDate = this.dateProvider.addDays(
      expires_refresh_token_days
    );

    await this.doctorTokenRepository.create({
      doctor_id: doctor.id,
      refresh_token: refreshToken,
      expires_date: refreshTokenExpiresDate,
    });

    const doctorAndToken: IResponse = {
      doctor: {
        name: doctor.name,
        email: doctor.email,
      },
      access_token: accessToken,
      refresh_token: refreshToken,
    };

    return doctorAndToken;
  }
}

export { AuthenticateDoctorService };

import { hash } from "bcrypt";
import { inject, injectable } from "tsyringe";

import { AppError } from "@common/error/AppError";
import { ICreateDoctorDTO } from "@domain/doctor/dto/ICreateDoctorDTO";
import { Doctor } from "@domain/doctor/entity/Doctor";
import { IDoctorRepository } from "@domain/doctor/repository/IDoctorRepository";

@injectable()
class CreateDoctorService {
  constructor(
    @inject("DoctorRepository")
    private doctorRepository: IDoctorRepository
  ) {}

  async execute({ name, email, password }: ICreateDoctorDTO): Promise<Doctor> {
    const doctorAlreadyExists = await this.doctorRepository.findByEmail(email);

    if (doctorAlreadyExists) {
      throw new AppError("Doctor already exists");
    }

    const passwordHash = await hash(password, 8);

    const doctor = await this.doctorRepository.create({
      name,
      email,
      password: passwordHash,
    });

    return doctor;
  }
}

export { CreateDoctorService };

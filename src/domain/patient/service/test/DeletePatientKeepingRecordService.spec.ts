import { v4 as uuid } from "uuid";

import { AppError } from "@common/error/AppError";
import { DayjsDateProvider } from "@common/provider/DateProvider/implementation/DayjsDateProvider";
import { AppointmentRepositoryTest } from "@domain/appointment/repository/test/AppointmentRepositoryTest";
import { CreateAppointmentService } from "@domain/appointment/service/CreateAppointmentService";
import { Doctor } from "@domain/doctor/entity/Doctor";
import { DoctorRepositoryTest } from "@domain/doctor/repository/test/DoctorRepositoryTest";
import { CreateDoctorService } from "@domain/doctor/service/CreateDoctorService";
import { Patient } from "@domain/patient/entity/Patient";
import { PatientRepositoryTest } from "@domain/patient/repository/test/PatientRepositoryTest";
import { CreatePatientService } from "@domain/patient/service/CreatePatientService";
import { DeletePatientKeepingRecordService } from "@domain/patient/service/DeletePatientKeepingRecordService";

let patientRepositoryTest: PatientRepositoryTest;
let createPatientService: CreatePatientService;
let deletePatientKeepingRecordService: DeletePatientKeepingRecordService;
let appointmentRepositoryTest: AppointmentRepositoryTest;
let createAppointmentService: CreateAppointmentService;
let dateProvider: DayjsDateProvider;
let createDoctorService: CreateDoctorService;
let doctorRepositoryTest: DoctorRepositoryTest;

let doctor: Doctor;
let patient: Patient;

describe("Delete Patient Keeping Record", () => {
  beforeEach(async () => {
    dateProvider = new DayjsDateProvider();

    doctorRepositoryTest = new DoctorRepositoryTest();
    createDoctorService = new CreateDoctorService(doctorRepositoryTest);

    patientRepositoryTest = new PatientRepositoryTest();
    createPatientService = new CreatePatientService(
      patientRepositoryTest,
      doctorRepositoryTest
    );

    appointmentRepositoryTest = new AppointmentRepositoryTest();
    deletePatientKeepingRecordService = new DeletePatientKeepingRecordService(
      patientRepositoryTest,
      appointmentRepositoryTest,
      doctorRepositoryTest
    );

    createAppointmentService = new CreateAppointmentService(
      appointmentRepositoryTest,
      patientRepositoryTest,
      dateProvider
    );

    const newDoctor = await createDoctorService.execute({
      name: "Doctor Test",
      email: "doctor@test.com",
      password: "123456",
    });

    doctor = newDoctor;

    const newPatient = await createPatientService.execute({
      doctor_id: doctor.id,
      name: "Patient Test",
      phone: "(48) 9 8875-0125",
      email: "patient@test.com",
      birth_date: "1985-07-16",
      gender: "M",
      height: 1.67,
      weight: 70.4,
    });

    patient = newPatient;
  });

  it("must delete a patient keeping records", async () => {
    expect(
      await deletePatientKeepingRecordService.execute({
        doctor_id: doctor.id,
        id: patient.id,
      })
    ).not.toBeInstanceOf(AppError);
  });

  it("must delete appointments comments to patients keeping records", async () => {
    const currentDate = new Date();
    const currentYear = currentDate.getFullYear() + 1;
    const currentMonth = currentDate.getMonth() + 1;
    const currentDay = currentDate.getDate();
    const appointmentDate = new Date(
      `${currentYear}-${currentMonth}-${currentDay} 09:00:00`
    );
    const appointmentDateTwo = new Date(
      `${currentYear}-${currentMonth}-${currentDay} 11:00:00`
    );

    await createAppointmentService.execute({
      doctor_id: doctor.id,
      patient_id: patient.id,
      date: appointmentDate,
      comment: "Comment 1",
    });

    await createAppointmentService.execute({
      doctor_id: doctor.id,
      patient_id: patient.id,
      date: appointmentDateTwo,
      comment: "Comment 2",
    });

    await deletePatientKeepingRecordService.execute({
      doctor_id: doctor.id,
      id: patient.id,
    });

    const appointments = await appointmentRepositoryTest.findByPatient(
      patient.id
    );

    appointments.forEach((appointmentTest) => {
      expect(appointmentTest.comment).toBeNull();
    });
  });

  it("should not delete non-existent patient", async () => {
    await expect(
      deletePatientKeepingRecordService.execute({
        doctor_id: doctor.id,
        id: uuid(),
      })
    ).rejects.toEqual(new AppError("Patient not found"));
  });

  it("should not delete non-existent patient", async () => {
    await expect(
      deletePatientKeepingRecordService.execute({
        doctor_id: uuid(),
        id: patient.id,
      })
    ).rejects.toEqual(new AppError("Doctor not found"));
  });
});

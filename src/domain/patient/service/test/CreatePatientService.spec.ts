import { v4 as uuid } from "uuid";

import { AppError } from "@common/error/AppError";
import { Doctor } from "@domain/doctor/entity/Doctor";
import { DoctorRepositoryTest } from "@domain/doctor/repository/test/DoctorRepositoryTest";
import { CreateDoctorService } from "@domain/doctor/service/CreateDoctorService";
import { Patient } from "@domain/patient/entity/Patient";
import { PatientRepositoryTest } from "@domain/patient/repository/test/PatientRepositoryTest";
import { CreatePatientService } from "@domain/patient/service/CreatePatientService";

let patientRepositoryTest: PatientRepositoryTest;
let createPatientService: CreatePatientService;
let doctorRepositoryTest: DoctorRepositoryTest;
let createDoctorService: CreateDoctorService;

let doctor: Doctor;

describe("Create Patient", () => {
  beforeEach(async () => {
    doctorRepositoryTest = new DoctorRepositoryTest();
    createDoctorService = new CreateDoctorService(doctorRepositoryTest);

    patientRepositoryTest = new PatientRepositoryTest();
    createPatientService = new CreatePatientService(
      patientRepositoryTest,
      doctorRepositoryTest
    );

    const newDoctor = await createDoctorService.execute({
      name: "Doctor Test",
      email: "doctor@test.com",
      password: "123456",
    });

    doctor = newDoctor;
  });

  it("must create a new patient", async () => {
    const patient = await createPatientService.execute({
      doctor_id: doctor.id,
      name: "Patient Test",
      phone: "(48) 9 8875-0125",
      email: "patient@test.com",
      birth_date: "1985-07-16",
      gender: "M",
      height: 1.67,
      weight: 70.4,
    });

    expect(patient).toBeInstanceOf(Patient);
    expect(patient).toHaveProperty("id");
  });

  it("should not create a new patient with non-existent doctor", async () => {
    await expect(
      createPatientService.execute({
        doctor_id: uuid(),
        name: "Patient Test",
        phone: "(48) 9 8875-0125",
        email: "patient@test.com",
        birth_date: "1985-07-16",
        gender: "M",
        height: 1.67,
        weight: 70.4,
      })
    ).rejects.toEqual(new AppError("Doctor not found"));
  });

  it("should not create a new patient with the same email", async () => {
    await createPatientService.execute({
      doctor_id: doctor.id,
      name: "Patient Test",
      phone: "(48) 9 8875-0125",
      email: "patient@test.com",
      birth_date: "1985-07-16",
      gender: "M",
      height: 1.67,
      weight: 70.4,
    });

    await expect(
      createPatientService.execute({
        doctor_id: doctor.id,
        name: "Patient Test",
        phone: "(48) 9 7875-0125",
        email: "patient@test.com",
        birth_date: "1985-07-16",
        gender: "M",
        height: 1.67,
        weight: 70.4,
      })
    ).rejects.toEqual(new AppError("Patient already exists"));
  });

  it("should not create a new patient with the same phone", async () => {
    await createPatientService.execute({
      doctor_id: doctor.id,
      name: "Patient Test",
      phone: "(48) 9 8875-0125",
      email: "patient@test.com",
      birth_date: "1985-07-16",
      gender: "M",
      height: 1.67,
      weight: 70.4,
    });

    await expect(
      createPatientService.execute({
        doctor_id: doctor.id,
        name: "Patient Test",
        phone: "(48) 9 8875-0125",
        email: "anotherpatient@test.com",
        birth_date: "1985-07-16",
        gender: "M",
        height: 1.67,
        weight: 70.4,
      })
    ).rejects.toEqual(new AppError("Patient already exists"));
  });
});

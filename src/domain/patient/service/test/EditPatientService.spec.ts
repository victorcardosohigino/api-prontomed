import { v4 as uuid } from "uuid";

import { AppError } from "@common/error/AppError";
import { Doctor } from "@domain/doctor/entity/Doctor";
import { DoctorRepositoryTest } from "@domain/doctor/repository/test/DoctorRepositoryTest";
import { CreateDoctorService } from "@domain/doctor/service/CreateDoctorService";
import { Patient } from "@domain/patient/entity/Patient";
import { PatientRepositoryTest } from "@domain/patient/repository/test/PatientRepositoryTest";
import { CreatePatientService } from "@domain/patient/service/CreatePatientService";
import { EditPatientService } from "@domain/patient/service/EditPatientService";

let createPatientService: CreatePatientService;
let patientRepositoryTest: PatientRepositoryTest;
let editPatientService: EditPatientService;

let doctorRepositoryTest: DoctorRepositoryTest;
let createDoctorService: CreateDoctorService;

let doctor: Doctor;
let patient: Patient;

describe("Edit Patient", () => {
  beforeEach(async () => {
    doctorRepositoryTest = new DoctorRepositoryTest();
    createDoctorService = new CreateDoctorService(doctorRepositoryTest);

    patientRepositoryTest = new PatientRepositoryTest();
    createPatientService = new CreatePatientService(
      patientRepositoryTest,
      doctorRepositoryTest
    );

    editPatientService = new EditPatientService(
      patientRepositoryTest,
      doctorRepositoryTest
    );

    const newDoctor = await createDoctorService.execute({
      name: "Doctor Test",
      email: "doctor@test.com",
      password: "123456",
    });

    doctor = newDoctor;

    const newPatient = await createPatientService.execute({
      doctor_id: doctor.id,
      name: "Patient Test",
      phone: "(48) 9 8875-0125",
      email: "patient@test.com",
      birth_date: "1985-07-16",
      gender: "M",
      height: 1.67,
      weight: 70.4,
    });

    patient = newPatient;
  });

  it("must edit a patient", async () => {
    const patientEdited = await editPatientService.execute({
      doctor_id: doctor.id,
      id: patient.id,
      name: "Patient Test Edited",
      phone: "(48) 9 8875-0126",
      email: "patientedited@test.com",
      birth_date: "1985-07-16",
      gender: "M",
      height: 1.67,
      weight: 70.4,
    });

    const patientFound = await patientRepositoryTest.findById(patient.id);

    expect(patientFound.name).toBe(patientEdited.name);
    expect(patientFound.email).toBe(patientEdited.email);
    expect(patientFound.phone).toBe(patientEdited.phone);
  });

  it("should not edit a patient with the same email", async () => {
    await createPatientService.execute({
      doctor_id: doctor.id,
      name: "Patient Test",
      phone: "(48) 9 8875-0127",
      email: "patientedited2@test.com",
      birth_date: "1985-07-16",
      gender: "M",
      height: 1.67,
      weight: 70.4,
    });

    await expect(
      editPatientService.execute({
        doctor_id: doctor.id,
        id: patient.id,
        name: "Patient Test",
        phone: "(48) 9 8875-0128",
        email: "patientedited2@test.com",
        birth_date: "1985-07-16",
        gender: "M",
        height: 1.67,
        weight: 70.4,
      })
    ).rejects.toEqual(new AppError("Phone or email already exists"));
  });

  it("should not edit a patient with the same email", async () => {
    await createPatientService.execute({
      doctor_id: doctor.id,
      name: "Patient Test",
      phone: "(48) 9 8875-0129",
      email: "patientedited3@test.com",
      birth_date: "1985-07-16",
      gender: "M",
      height: 1.67,
      weight: 70.4,
    });

    await expect(
      editPatientService.execute({
        doctor_id: doctor.id,
        id: patient.id,
        name: "Patient Test",
        phone: "(48) 9 8875-0129",
        email: "patientedited3@test.com",
        birth_date: "1985-07-16",
        gender: "M",
        height: 1.67,
        weight: 70.4,
      })
    ).rejects.toEqual(new AppError("Phone or email already exists"));
  });

  it("should not edit a non-existent doctor", async () => {
    await expect(
      editPatientService.execute({
        doctor_id: uuid(),
        id: patient.id,
        name: "Patient Test",
        phone: "(48) 9 8875-0125",
        email: "patientedited@test.com",
        birth_date: "1985-07-16",
        gender: "M",
        height: 1.67,
        weight: 70.4,
      })
    ).rejects.toEqual(new AppError("Doctor not found"));
  });

  it("should not edit a non-existent patient", async () => {
    await expect(
      editPatientService.execute({
        doctor_id: doctor.id,
        id: uuid(),
        name: "Patient Test",
        phone: "(48) 9 8875-0125",
        email: "patientedited@test.com",
        birth_date: "1985-07-16",
        gender: "M",
        height: 1.67,
        weight: 70.4,
      })
    ).rejects.toEqual(new AppError("Patient not found"));
  });
});

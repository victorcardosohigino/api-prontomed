import { v4 as uuid } from "uuid";

import { AppError } from "@common/error/AppError";
import { Doctor } from "@domain/doctor/entity/Doctor";
import { DoctorRepositoryTest } from "@domain/doctor/repository/test/DoctorRepositoryTest";
import { CreateDoctorService } from "@domain/doctor/service/CreateDoctorService";
import { Patient } from "@domain/patient/entity/Patient";
import { PatientRepositoryTest } from "@domain/patient/repository/test/PatientRepositoryTest";
import { CreatePatientService } from "@domain/patient/service/CreatePatientService";
import { DeletePatientService } from "@domain/patient/service/DeletePatientService";
import { ListPatientService } from "@domain/patient/service/ListPatientService";

let createPatientService: CreatePatientService;
let patientRepositoryTest: PatientRepositoryTest;
let listPatientService: ListPatientService;
let deletePatientService: DeletePatientService;
let doctorRepositoryTest: DoctorRepositoryTest;
let createDoctorService: CreateDoctorService;

let doctor: Doctor;
let patient: Patient;

describe("Lis Patients", () => {
  beforeAll(async () => {
    doctorRepositoryTest = new DoctorRepositoryTest();
    createDoctorService = new CreateDoctorService(doctorRepositoryTest);

    patientRepositoryTest = new PatientRepositoryTest();
    createPatientService = new CreatePatientService(
      patientRepositoryTest,
      doctorRepositoryTest
    );

    listPatientService = new ListPatientService(
      patientRepositoryTest,
      doctorRepositoryTest
    );

    deletePatientService = new DeletePatientService(
      patientRepositoryTest,
      doctorRepositoryTest
    );

    const newDoctor = await createDoctorService.execute({
      name: "Doctor Test",
      email: "doctor@test.com",
      password: "123456",
    });

    doctor = newDoctor;

    const newPatient = await createPatientService.execute({
      doctor_id: doctor.id,
      name: "Patient Test",
      phone: "(48) 9 8875-0125",
      email: "patient@test.com",
      birth_date: "1985-07-16",
      gender: "M",
      height: 1.67,
      weight: 70.4,
    });

    patient = newPatient;
  });

  it("must list a patient", async () => {
    const patientFounds = await listPatientService.execute({
      doctor_id: doctor.id,
      offset: 0,
      limit: 1,
    });

    expect(patientFounds[0].id).toBe(patient.id);
    expect(patientFounds.length).toBe(1);
  });

  it("should not list a patient with non-existent doctor", async () => {
    await expect(
      listPatientService.execute({
        doctor_id: uuid(),
        offset: 0,
        limit: 1,
      })
    ).rejects.toEqual(new AppError("Doctor not found"));
  });

  it("should not list a patient", async () => {
    await deletePatientService.execute({
      doctor_id: doctor.id,
      id: patient.id,
    });
    const patientFounds = await listPatientService.execute({
      doctor_id: doctor.id,
      offset: 0,
      limit: 1,
    });

    expect(patientFounds.length).toBe(0);
  });
});

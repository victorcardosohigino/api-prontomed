import { v4 as uuid } from "uuid";

import { AppError } from "@common/error/AppError";
import { Doctor } from "@domain/doctor/entity/Doctor";
import { DoctorRepositoryTest } from "@domain/doctor/repository/test/DoctorRepositoryTest";
import { CreateDoctorService } from "@domain/doctor/service/CreateDoctorService";
import { Patient } from "@domain/patient/entity/Patient";
import { PatientRepositoryTest } from "@domain/patient/repository/test/PatientRepositoryTest";
import { CreatePatientService } from "@domain/patient/service/CreatePatientService";
import { FindPatientService } from "@domain/patient/service/FindPatientService";

let createPatientService: CreatePatientService;
let patientRepositoryTest: PatientRepositoryTest;
let findPatientService: FindPatientService;
let doctorRepositoryTest: DoctorRepositoryTest;
let createDoctorService: CreateDoctorService;

let doctor: Doctor;
let patient: Patient;

describe("Find Patient", () => {
  beforeAll(async () => {
    doctorRepositoryTest = new DoctorRepositoryTest();
    createDoctorService = new CreateDoctorService(doctorRepositoryTest);

    patientRepositoryTest = new PatientRepositoryTest();
    createPatientService = new CreatePatientService(
      patientRepositoryTest,
      doctorRepositoryTest
    );
    findPatientService = new FindPatientService(
      patientRepositoryTest,
      doctorRepositoryTest
    );

    const newDoctor = await createDoctorService.execute({
      name: "Doctor Test",
      email: "doctor@test.com",
      password: "123456",
    });

    doctor = newDoctor;

    const newPatient = await createPatientService.execute({
      doctor_id: doctor.id,
      name: "Patient Test",
      phone: "(48) 9 8875-0125",
      email: "patient@test.com",
      birth_date: "1985-07-16",
      gender: "M",
      height: 1.67,
      weight: 70.4,
    });

    patient = newPatient;
  });

  it("must find a patient", async () => {
    const patientFounds = await findPatientService.execute({
      doctor_id: doctor.id,
      name: patient.name,
      phone: patient.phone,
      email: patient.email,
    });

    expect(patientFounds[0].id).toBe(patient.id);
    expect(patientFounds.length).toBe(1);
  });

  it("should not find a patient", async () => {
    const patientFounds = await findPatientService.execute({
      doctor_id: doctor.id,
      name: "Patient Test 2",
      phone: "(48) 9 8875-0126",
      email: "patientFind@test.com",
    });

    expect(patientFounds.length).toBe(0);
  });

  it("should not find a patient with non-existent doctor", async () => {
    await expect(
      findPatientService.execute({
        doctor_id: uuid(),
        name: "Patient Test 2",
        phone: "(48) 9 8875-0126",
        email: "patientFind@test.com",
      })
    ).rejects.toEqual(new AppError("Doctor not found"));
  });
});

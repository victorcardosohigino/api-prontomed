import { v4 as uuid } from "uuid";

import { AppError } from "@common/error/AppError";
import { Doctor } from "@domain/doctor/entity/Doctor";
import { DoctorRepositoryTest } from "@domain/doctor/repository/test/DoctorRepositoryTest";
import { CreateDoctorService } from "@domain/doctor/service/CreateDoctorService";
import { Patient } from "@domain/patient/entity/Patient";
import { PatientRepositoryTest } from "@domain/patient/repository/test/PatientRepositoryTest";
import { CreatePatientService } from "@domain/patient/service/CreatePatientService";
import { DeletePatientService } from "@domain/patient/service/DeletePatientService";

let patientRepositoryTest: PatientRepositoryTest;
let createPatientService: CreatePatientService;
let deletePatientService: DeletePatientService;
let doctorRepositoryTest: DoctorRepositoryTest;
let createDoctorService: CreateDoctorService;

let patient: Patient;
let doctor: Doctor;

describe("Delete Patient", () => {
  beforeEach(async () => {
    doctorRepositoryTest = new DoctorRepositoryTest();
    createDoctorService = new CreateDoctorService(doctorRepositoryTest);

    patientRepositoryTest = new PatientRepositoryTest();
    createPatientService = new CreatePatientService(
      patientRepositoryTest,
      doctorRepositoryTest
    );

    deletePatientService = new DeletePatientService(
      patientRepositoryTest,
      doctorRepositoryTest
    );

    const newDoctor = await createDoctorService.execute({
      name: "Doctor Test",
      email: "doctor@test.com",
      password: "123456",
    });

    doctor = newDoctor;

    const newPatient = await createPatientService.execute({
      doctor_id: doctor.id,
      name: "Patient Test",
      phone: "(48) 9 8875-0125",
      email: "patient@test.com",
      birth_date: "1985-07-16",
      gender: "M",
      height: 1.67,
      weight: 70.4,
    });

    patient = newPatient;
  });

  it("must delete a patient", async () => {
    expect(
      await deletePatientService.execute({
        doctor_id: doctor.id,
        id: patient.id,
      })
    ).not.toBeInstanceOf(AppError);
  });

  it("should not delete a non-existent doctor", async () => {
    await expect(
      deletePatientService.execute({
        doctor_id: uuid(),
        id: patient.id,
      })
    ).rejects.toEqual(new AppError("Doctor not found"));
  });

  it("should not delete a non-existent patient", async () => {
    await expect(
      deletePatientService.execute({
        doctor_id: doctor.id,
        id: uuid(),
      })
    ).rejects.toEqual(new AppError("Patient not found"));
  });
});

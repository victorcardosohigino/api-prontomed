import { inject, injectable } from "tsyringe";

import { AppError } from "@common/error/AppError";
import { IDoctorRepository } from "@domain/doctor/repository/IDoctorRepository";
import { IListPatientDTO } from "@domain/patient/dto/IListPatientDTO";
import { Patient } from "@domain/patient/entity/Patient";
import { IPatientRepository } from "@domain/patient/repository/IPatientRepository";

@injectable()
class ListPatientService {
  constructor(
    @inject("PatientRepository")
    private patientRepository: IPatientRepository,
    @inject("DoctorRepository")
    private doctorRepository: IDoctorRepository
  ) {}

  async execute({
    doctor_id,
    offset,
    limit,
  }: IListPatientDTO): Promise<Patient[]> {
    const doctor = await this.doctorRepository.findById(doctor_id);

    if (!doctor) {
      throw new AppError("Doctor not found");
    }

    const patients = await this.patientRepository.findByDoctor({
      doctor_id,
      offset,
      limit,
    });

    return patients;
  }
}

export { ListPatientService };

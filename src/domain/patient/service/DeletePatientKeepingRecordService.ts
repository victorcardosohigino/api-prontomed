import { inject, injectable } from "tsyringe";

import { AppError } from "@common/error/AppError";
import { IAppointmentRepository } from "@domain/appointment/repository/IAppointmentRepository";
import { IDoctorRepository } from "@domain/doctor/repository/IDoctorRepository";
import { IDeletePatientDTO } from "@domain/patient/dto/IDeletePatientDTO";
import { IPatientRepository } from "@domain/patient/repository/IPatientRepository";

@injectable()
class DeletePatientKeepingRecordService {
  constructor(
    @inject("PatientRepository")
    private patientRepository: IPatientRepository,
    @inject("AppointmentRepository")
    private appointmentRepository: IAppointmentRepository,
    @inject("DoctorRepository")
    private doctorRepository: IDoctorRepository
  ) {}

  async execute({ id, doctor_id }: IDeletePatientDTO): Promise<void> {
    const doctor = await this.doctorRepository.findById(doctor_id);

    if (!doctor) {
      throw new AppError("Doctor not found");
    }

    const patient = await this.patientRepository.findByIdAndDoctor({
      id,
      doctor_id,
    });

    if (!patient) {
      throw new AppError("Patient not found");
    }

    await this.appointmentRepository.deleteKeepingRecord(id);

    Object.assign(patient, {
      name: "deleted",
      phone: "deleted",
      email: "deleted",
      birth_date: new Date(),
      active: false,
    });

    await this.patientRepository.save(patient);
  }
}

export { DeletePatientKeepingRecordService };

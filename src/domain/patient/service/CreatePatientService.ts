import { inject, injectable } from "tsyringe";

import { AppError } from "@common/error/AppError";
import { IDoctorRepository } from "@domain/doctor/repository/IDoctorRepository";
import { ICreatePatientDTO } from "@domain/patient/dto/ICreatePatientDTO";
import { Patient } from "@domain/patient/entity/Patient";
import { IPatientRepository } from "@domain/patient/repository/IPatientRepository";

@injectable()
class CreatePatientService {
  constructor(
    @inject("PatientRepository")
    private patientRepository: IPatientRepository,
    @inject("DoctorRepository")
    private doctorRepository: IDoctorRepository
  ) {}

  async execute({
    doctor_id,
    name,
    phone,
    email,
    birth_date,
    gender,
    height,
    weight,
  }: ICreatePatientDTO): Promise<Patient> {
    const doctor = await this.doctorRepository.findById(doctor_id);

    if (!doctor) {
      throw new AppError("Doctor not found");
    }

    const patientAlreadyExists =
      await this.patientRepository.findByDoctorAndPhoneOrEmail({
        doctor_id,
        phone,
        email,
      });

    if (patientAlreadyExists) {
      throw new AppError("Patient already exists");
    }

    const birthDateFormated = new Date(birth_date).toISOString();

    const patient = await this.patientRepository.create({
      doctor_id,
      name,
      phone,
      email,
      birth_date: birthDateFormated,
      gender,
      height,
      weight,
    });

    return patient;
  }
}

export { CreatePatientService };

import { inject, injectable } from "tsyringe";

import { AppError } from "@common/error/AppError";
import { IDoctorRepository } from "@domain/doctor/repository/IDoctorRepository";
import { IEditPatientDTO } from "@domain/patient/dto/IEditPatientDTO";
import { Patient } from "@domain/patient/entity/Patient";
import { IPatientRepository } from "@domain/patient/repository/IPatientRepository";

@injectable()
class EditPatientService {
  constructor(
    @inject("PatientRepository")
    private patientRepository: IPatientRepository,
    @inject("DoctorRepository")
    private doctorRepository: IDoctorRepository
  ) {}

  async execute({
    id,
    doctor_id,
    name,
    phone,
    email,
    birth_date,
    gender,
    height,
    weight,
  }: IEditPatientDTO): Promise<Patient> {
    const doctor = await this.doctorRepository.findById(doctor_id);

    if (!doctor) {
      throw new AppError("Doctor not found");
    }

    const patient = await this.patientRepository.findById(id);

    if (!patient) {
      throw new AppError("Patient not found");
    }

    const patientPhoneOrEmailAlreadyExists =
      await this.patientRepository.findByDoctorAndPhoneOrEmail({
        doctor_id,
        phone,
        email,
      });

    if (patientPhoneOrEmailAlreadyExists) {
      if (patientPhoneOrEmailAlreadyExists.id !== id) {
        throw new AppError("Phone or email already exists");
      }
    }

    const birthDateFormated = new Date(birth_date).toISOString();

    Object.assign(patient, {
      name,
      phone,
      email,
      birth_date: birthDateFormated,
      gender,
      height,
      weight,
    });

    await this.patientRepository.save(patient);

    return patient;
  }
}

export { EditPatientService };

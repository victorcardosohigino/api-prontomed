import { inject, injectable } from "tsyringe";

import { AppError } from "@common/error/AppError";
import { IDoctorRepository } from "@domain/doctor/repository/IDoctorRepository";
import { IDeletePatientDTO } from "@domain/patient/dto/IDeletePatientDTO";
import { IPatientRepository } from "@domain/patient/repository/IPatientRepository";

@injectable()
class DeletePatientService {
  constructor(
    @inject("PatientRepository")
    private patientRepository: IPatientRepository,
    @inject("DoctorRepository")
    private doctorRepository: IDoctorRepository
  ) {}

  async execute({ id, doctor_id }: IDeletePatientDTO): Promise<void> {
    const doctor = await this.doctorRepository.findById(doctor_id);

    if (!doctor) {
      throw new AppError("Doctor not found");
    }

    const patient = await this.patientRepository.findById(id);

    if (!patient) {
      throw new AppError("Patient not found");
    }

    this.patientRepository.delete(id);
  }
}

export { DeletePatientService };

import { inject, injectable } from "tsyringe";

import { AppError } from "@common/error/AppError";
import { IDoctorRepository } from "@domain/doctor/repository/IDoctorRepository";
import { IFindByDoctorAndNameOrPhoneOrEmailDTO } from "@domain/patient/dto/IFindByDoctorAndNameOrPhoneOrEmailDTO";
import { Patient } from "@domain/patient/entity/Patient";
import { IPatientRepository } from "@domain/patient/repository/IPatientRepository";

@injectable()
class FindPatientService {
  constructor(
    @inject("PatientRepository")
    private patientRepository: IPatientRepository,
    @inject("DoctorRepository")
    private doctorRepository: IDoctorRepository
  ) {}

  async execute({
    doctor_id,
    name,
    phone,
    email,
  }: IFindByDoctorAndNameOrPhoneOrEmailDTO): Promise<Patient[]> {
    const doctor = await this.doctorRepository.findById(doctor_id);

    if (!doctor) {
      throw new AppError("Doctor not found");
    }

    const patients =
      await this.patientRepository.findByDoctorAndNameOrPhoneOrEmail({
        doctor_id,
        name,
        phone,
        email,
      });

    return patients;
  }
}

export { FindPatientService };

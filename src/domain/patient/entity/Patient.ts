import {
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryColumn,
} from "typeorm";
import { v4 as uuidV4 } from "uuid";

import { Doctor } from "@domain/doctor/entity/Doctor";

@Entity("patient")
class Patient {
  @PrimaryColumn()
  id: string;

  @Column()
  doctor_id: string;

  @ManyToOne(() => Doctor)
  @JoinColumn({ name: "doctor_id" })
  doctor: Doctor;

  @Column({ length: 250 })
  name: string;

  @Column({ length: 16 })
  phone: string;

  @Column({ length: 250 })
  email: string;

  @Column("date")
  birth_date: Date;

  @Column({ length: 1 })
  gender: string;

  @Column("float")
  height: number;

  @Column("float")
  weight: number;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @Column()
  active: boolean;

  constructor() {
    if (!this.id) {
      this.id = uuidV4();
    }
  }
}

export { Patient };

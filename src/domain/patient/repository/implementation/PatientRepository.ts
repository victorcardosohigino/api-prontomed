import { getRepository, Repository } from "typeorm";

import { ICreatePatientDTO } from "@domain/patient/dto/ICreatePatientDTO";
import { IFindByDoctorAndNameOrPhoneOrEmailDTO } from "@domain/patient/dto/IFindByDoctorAndNameOrPhoneOrEmailDTO";
import { IFindByDoctorAndPhoneOrEmailDTO } from "@domain/patient/dto/IFindByDoctorAndPhoneOrEmailDTO";
import { IFindByIdAndDoctorDTO } from "@domain/patient/dto/IFindByIdAndDoctorDTO";
import { IListPatientDTO } from "@domain/patient/dto/IListPatientDTO";
import { Patient } from "@domain/patient/entity/Patient";
import { IPatientRepository } from "@domain/patient/repository/IPatientRepository";

class PatientRepository implements IPatientRepository {
  private repository: Repository<Patient>;

  constructor() {
    this.repository = getRepository(Patient);
  }

  async create({
    doctor_id,
    name,
    phone,
    email,
    birth_date,
    gender,
    height,
    weight,
  }: ICreatePatientDTO): Promise<Patient> {
    const patient = this.repository.create({
      doctor_id,
      name,
      phone,
      email,
      birth_date,
      gender,
      height,
      weight,
    });

    await this.repository.save(patient);

    return patient;
  }

  async findById(id: string): Promise<Patient> {
    const patient = await this.repository.findOne({
      id,
      active: true,
    });
    return patient;
  }

  async findByDoctorAndPhoneOrEmail({
    doctor_id,
    phone,
    email,
  }: IFindByDoctorAndPhoneOrEmailDTO): Promise<Patient> {
    const where = [
      { doctor_id, phone, active: true },
      { doctor_id, email, active: true },
    ];
    const patient = await this.repository.findOne({
      where,
    });
    return patient;
  }

  async findByDoctorAndNameOrPhoneOrEmail({
    doctor_id,
    name,
    phone,
    email,
  }: IFindByDoctorAndNameOrPhoneOrEmailDTO): Promise<Patient[]> {
    const qb = this.repository
      .createQueryBuilder()
      .select([
        "id",
        "doctor_id",
        "name",
        "phone",
        "email",
        "birth_date",
        "gender",
        "height",
        "weight",
        "created_at",
        "updated_at",
        "active",
      ])
      .orderBy("name", "ASC")
      .where({
        doctor_id,
        active: true,
      });
    if (name) {
      qb.andWhere("upper(name) like upper(:name)", { name: `%${name}%` });
    }

    if (phone) {
      qb.andWhere("phone like :phone", { phone: `%${phone}%` });
    }

    if (email) {
      qb.andWhere("email = :email", { email });
    }

    const patients = await qb.getRawMany();

    return patients;
  }

  async delete(id: string): Promise<void> {
    this.repository.update({ id }, { active: false });
  }

  async save(patient: Patient): Promise<void> {
    this.repository.save(patient);
  }

  async findByDoctor({
    doctor_id,
    offset,
    limit,
  }: IListPatientDTO): Promise<Patient[]> {
    const where = {
      where: { doctor_id, active: true },
    };

    if (offset) {
      Object.assign(where, { skip: offset });
    }

    if (limit) {
      Object.assign(where, { take: limit });
    }

    const patients = await this.repository.find(where);

    return patients;
  }

  async findByIdAndDoctor({
    id,
    doctor_id,
  }: IFindByIdAndDoctorDTO): Promise<Patient> {
    const patient = await this.repository.findOne({
      id,
      doctor_id,
      active: true,
    });
    return patient;
  }
}

export { PatientRepository };

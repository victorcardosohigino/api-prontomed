import { ICreatePatientDTO } from "@domain/patient/dto/ICreatePatientDTO";
import { IFindByDoctorAndNameOrPhoneOrEmailDTO } from "@domain/patient/dto/IFindByDoctorAndNameOrPhoneOrEmailDTO";
import { IFindByDoctorAndPhoneOrEmailDTO } from "@domain/patient/dto/IFindByDoctorAndPhoneOrEmailDTO";
import { IFindByIdAndDoctorDTO } from "@domain/patient/dto/IFindByIdAndDoctorDTO";
import { IListPatientDTO } from "@domain/patient/dto/IListPatientDTO";
import { Patient } from "@domain/patient/entity/Patient";

interface IPatientRepository {
  create(data: ICreatePatientDTO): Promise<Patient>;
  findById(id: string): Promise<Patient>;
  findByDoctorAndPhoneOrEmail(
    data: IFindByDoctorAndPhoneOrEmailDTO
  ): Promise<Patient>;
  findByDoctorAndNameOrPhoneOrEmail(
    data: IFindByDoctorAndNameOrPhoneOrEmailDTO
  ): Promise<Patient[]>;
  delete(id: string): Promise<void>;
  save(patient: Patient): Promise<void>;
  findByDoctor(data: IListPatientDTO): Promise<Patient[]>;
  findByIdAndDoctor(data: IFindByIdAndDoctorDTO): Promise<Patient>;
}

export { IPatientRepository };

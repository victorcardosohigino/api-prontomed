import { ICreatePatientDTO } from "@domain/patient/dto/ICreatePatientDTO";
import { IFindByDoctorAndNameOrPhoneOrEmailDTO } from "@domain/patient/dto/IFindByDoctorAndNameOrPhoneOrEmailDTO";
import { IFindByDoctorAndPhoneOrEmailDTO } from "@domain/patient/dto/IFindByDoctorAndPhoneOrEmailDTO";
import { IFindByIdAndDoctorDTO } from "@domain/patient/dto/IFindByIdAndDoctorDTO";
import { IListPatientDTO } from "@domain/patient/dto/IListPatientDTO";
import { Patient } from "@domain/patient/entity/Patient";
import { IPatientRepository } from "@domain/patient/repository/IPatientRepository";

class PatientRepositoryTest implements IPatientRepository {
  patients: Patient[] = [];

  async create({
    doctor_id,
    name,
    phone,
    email,
    birth_date,
    gender,
    height,
    weight,
  }: ICreatePatientDTO): Promise<Patient> {
    const patient = new Patient();

    Object.assign(patient, {
      doctor_id,
      name,
      phone,
      email,
      birth_date,
      gender,
      height,
      weight,
    });

    this.patients.push(patient);

    return patient;
  }

  async findById(id: string): Promise<Patient> {
    const patient = this.patients.find((patient) => patient.id === id);

    return patient;
  }

  async findByDoctorAndPhoneOrEmail({
    doctor_id,
    phone,
    email,
  }: IFindByDoctorAndPhoneOrEmailDTO): Promise<Patient> {
    const patient = this.patients.find(
      (patient) =>
        (patient.doctor_id === doctor_id && patient.phone === phone) ||
        patient.email === email
    );

    return patient;
  }

  async findByDoctorAndNameOrPhoneOrEmail({
    doctor_id,
    name,
    phone,
    email,
  }: IFindByDoctorAndNameOrPhoneOrEmailDTO): Promise<Patient[]> {
    const patients = this.patients.filter(
      (patient) =>
        (patient.doctor_id === doctor_id &&
          patient.name.toUpperCase() === name.toUpperCase()) ||
        patient.phone === phone ||
        patient.email.toUpperCase() === email.toUpperCase()
    );

    return patients;
  }

  async delete(id: string): Promise<void> {
    const patient = this.patients.find((patient) => patient.id === id);

    this.patients.splice(this.patients.indexOf(patient));
  }

  async save(patient: Patient): Promise<void> {
    const index = this.patients.findIndex(
      (patientIndex) => patientIndex.id === patient.id
    );

    this.patients[index] = patient;
  }

  async findByDoctor({
    doctor_id,
    offset,
    limit,
  }: IListPatientDTO): Promise<Patient[]> {
    const patients = this.patients.filter(
      (patientTest) => patientTest.doctor_id === doctor_id
    );

    if (offset && limit) {
      return patients.slice(offset, limit + 1);
    }

    return patients;
  }

  async findByIdAndDoctor({
    id,
    doctor_id,
  }: IFindByIdAndDoctorDTO): Promise<Patient> {
    const patient = this.patients.find(
      (patient) => patient.id === id && patient.doctor_id === doctor_id
    );

    return patient;
  }
}

export { PatientRepositoryTest };

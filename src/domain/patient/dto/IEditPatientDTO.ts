interface IEditPatientDTO {
  id: string;
  doctor_id: string;
  name: string;
  phone: string;
  email: string;
  birth_date: string;
  gender: string;
  height: number;
  weight: number;
}

export { IEditPatientDTO };

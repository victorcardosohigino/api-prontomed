interface IFindByDoctorAndPhoneOrEmailDTO {
  doctor_id: string;
  phone?: string;
  email?: string;
}

export { IFindByDoctorAndPhoneOrEmailDTO };

interface IDeletePatientDTO {
  id: string;
  doctor_id: string;
}

export { IDeletePatientDTO };

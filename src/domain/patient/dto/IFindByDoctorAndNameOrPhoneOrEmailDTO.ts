interface IFindByDoctorAndNameOrPhoneOrEmailDTO {
  doctor_id: string;
  name?: string;
  phone?: string;
  email?: string;
}

export { IFindByDoctorAndNameOrPhoneOrEmailDTO };

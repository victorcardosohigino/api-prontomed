interface IFindByIdAndDoctorDTO {
  id: string;
  doctor_id: string;
}

export { IFindByIdAndDoctorDTO };

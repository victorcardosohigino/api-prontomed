interface IListPatientDTO {
  doctor_id: string;
  offset?: number;
  limit?: number;
}

export { IListPatientDTO };

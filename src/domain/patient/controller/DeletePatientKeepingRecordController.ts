import { Request, Response } from "express";
import { container } from "tsyringe";

import { DeletePatientKeepingRecordService } from "@domain/patient/service/DeletePatientKeepingRecordService";

class DeletePatientKeepingRecordController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { id } = request.params;
    const doctor_id = request.doctor.doctor_id as string;

    const deletePatientKeepingRecordService = container.resolve(
      DeletePatientKeepingRecordService
    );

    await deletePatientKeepingRecordService.execute({ id, doctor_id });

    return response.status(204).send();
  }
}

export { DeletePatientKeepingRecordController };

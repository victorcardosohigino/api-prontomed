import { Request, Response } from "express";
import { container } from "tsyringe";

import { CreatePatientService } from "@domain/patient/service/CreatePatientService";

class CreatePatientController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { name, phone, email, birth_date, gender, height, weight } =
      request.body;
    const doctor_id = request.doctor.doctor_id as string;

    const createPatientService = container.resolve(CreatePatientService);

    const patient = await createPatientService.execute({
      doctor_id,
      name,
      phone,
      email,
      birth_date,
      gender,
      height,
      weight,
    });

    return response.status(201).send(patient);
  }
}

export { CreatePatientController };

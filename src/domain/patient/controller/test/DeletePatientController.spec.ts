import { hash } from "bcrypt";
import request from "supertest";
import { Connection } from "typeorm";
import { v4 as uuid } from "uuid";

import createConnection from "@common/database/connection";
import { app } from "@common/http/app";

let connection: Connection;

describe("DELETE /patient/:id", () => {
  beforeAll(async () => {
    connection = await createConnection();
    await connection.runMigrations();

    const id = uuid();
    const password = await hash("123456", 8);

    await connection.query(
      `INSERT INTO doctor(id, name, email, password, active, created_at, updated_at)
        values('${id}', 'Doctor Test', 'doctor@test.com', '${password}', true, 'now()', 'now()')`
    );
  });

  afterAll(async () => {
    await connection.dropDatabase();
    await connection.close();
  });

  it("must delete a new patient", async () => {
    const responseAuth = await request(app)
      .post("/authentication/session")
      .send({
        email: "doctor@test.com",
        password: "123456",
      });

    const { access_token } = responseAuth.body;

    await request(app)
      .post("/patient")
      .send({
        name: "Patient Test",
        phone: "(48) 9 8875-0125",
        email: "patient@test.com",
        birth_date: "1985-07-16",
        gender: "M",
        height: 1.67,
        weight: 70.4,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    const responseList = await request(app)
      .get("/patient")
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    const patients = responseList.body;

    const responseDelete = await request(app)
      .delete(`/patient/${patients[0].id}`)
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    expect(responseDelete.status).toBe(204);
  });

  it("should not delete a non-existent patient", async () => {
    const responseAuth = await request(app)
      .post("/authentication/session")
      .send({
        email: "doctor@test.com",
        password: "123456",
      });

    const { access_token } = responseAuth.body;

    const responseDelete = await request(app)
      .delete(`/patient/${uuid()}`)
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    expect(responseDelete.status).toBe(400);
  });
});

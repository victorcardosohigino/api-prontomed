import { hash } from "bcrypt";
import request from "supertest";
import { Connection } from "typeorm";
import { v4 as uuid } from "uuid";

import createConnection from "@common/database/connection";
import { app } from "@common/http/app";

let connection: Connection;

describe("GET /patient/list", () => {
  beforeAll(async () => {
    connection = await createConnection();
    await connection.runMigrations();

    const id = uuid();
    const password = await hash("123456", 8);

    await connection.query(
      `INSERT INTO doctor(id, name, email, password, active, created_at, updated_at)
        values('${id}', 'Doctor Test', 'doctor@test.com', '${password}', true, 'now()', 'now()')`
    );
  });

  afterAll(async () => {
    await connection.dropDatabase();
    await connection.close();
  });

  it("must list a patient ", async () => {
    const responseAuth = await request(app)
      .post("/authentication/session")
      .send({
        email: "doctor@test.com",
        password: "123456",
      });

    const { access_token } = responseAuth.body;

    await request(app)
      .post("/patient")
      .send({
        name: "Patient Test",
        phone: "(48) 9 8875-0125",
        email: "patient@test.com",
        birth_date: "1985-07-16",
        gender: "M",
        height: 1.67,
        weight: 70.4,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    const responseList = await request(app)
      .get("/patient/list")
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    expect(responseList.status).toBe(200);
    expect(responseList.body.length).toBe(1);
    expect(responseList.body[0]).toHaveProperty("id");
    expect(responseList.body[0].email).toEqual("patient@test.com");
  });

  it("must list a patient by offset and ", async () => {
    const responseAuth = await request(app)
      .post("/authentication/session")
      .send({
        email: "doctor@test.com",
        password: "123456",
      });

    const { access_token } = responseAuth.body;

    await request(app)
      .post("/patient")
      .send({
        name: "Patient Test",
        phone: "(48) 9 8875-0125",
        email: "patient@test.com",
        birth_date: "1985-07-16",
        gender: "M",
        height: 1.67,
        weight: 70.4,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    const responseList = await request(app)
      .get("/patient/list")
      .query({
        offset: 0,
        limit: 1,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    expect(responseList.status).toBe(200);
    expect(responseList.body.length).toBe(1);
    expect(responseList.body[0]).toHaveProperty("id");
    expect(responseList.body[0].name).toEqual("Patient Test");
  });
});

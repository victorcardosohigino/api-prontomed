import { hash } from "bcrypt";
import request from "supertest";
import { Connection } from "typeorm";
import { v4 as uuid } from "uuid";

import createConnection from "@common/database/connection";
import { app } from "@common/http/app";

let connection: Connection;

describe("GET /patient", () => {
  beforeAll(async () => {
    connection = await createConnection();
    await connection.runMigrations();

    const id = uuid();
    const password = await hash("123456", 8);

    await connection.query(
      `INSERT INTO doctor(id, name, email, password, active, created_at, updated_at)
        values('${id}', 'Doctor Test', 'doctor@test.com', '${password}', true, 'now()', 'now()')`
    );
  });

  afterAll(async () => {
    await connection.dropDatabase();
    await connection.close();
  });

  it("must find a patient ", async () => {
    const responseAuth = await request(app)
      .post("/authentication/session")
      .send({
        email: "doctor@test.com",
        password: "123456",
      });

    const { access_token } = responseAuth.body;

    await request(app)
      .post("/patient")
      .send({
        name: "Patient Test",
        phone: "(48) 9 8875-0125",
        email: "patient@test.com",
        birth_date: "1985-07-16",
        gender: "M",
        height: 1.67,
        weight: 70.4,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    const responseFind = await request(app)
      .get("/patient")
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    expect(responseFind.status).toBe(200);
    expect(responseFind.body.length).toBe(1);
    expect(responseFind.body[0]).toHaveProperty("id");
    expect(responseFind.body[0].email).toEqual("patient@test.com");
  });

  it("must find a patient by name", async () => {
    const responseAuth = await request(app)
      .post("/authentication/session")
      .send({
        email: "doctor@test.com",
        password: "123456",
      });

    const { access_token } = responseAuth.body;

    const responsePatient = await request(app)
      .post("/patient")
      .send({
        name: "Patient Test",
        phone: "(48) 9 8875-0125",
        email: "patient@test.com",
        birth_date: "1985-07-16",
        gender: "M",
        height: 1.67,
        weight: 70.4,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    const { name } = responsePatient.body;

    const responseFind = await request(app)
      .get("/patient")
      .query({
        name,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    expect(responseFind.status).toBe(200);
    expect(responseFind.body.length).toBe(1);
    expect(responseFind.body[0]).toHaveProperty("id");
    expect(responseFind.body[0].name).toEqual("Patient Test");
  });

  it("must find a patient by email", async () => {
    const responseAuth = await request(app)
      .post("/authentication/session")
      .send({
        email: "doctor@test.com",
        password: "123456",
      });

    const { access_token } = responseAuth.body;

    const responsePatient = await request(app)
      .post("/patient")
      .send({
        name: "Patient Test",
        phone: "(48) 9 8875-0125",
        email: "patient@test.com",
        birth_date: "1985-07-16",
        gender: "M",
        height: 1.67,
        weight: 70.4,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    const { email } = responsePatient.body;

    const responseFind = await request(app)
      .get("/patient")
      .query({
        email,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    expect(responseFind.status).toBe(200);
    expect(responseFind.body.length).toBe(1);
    expect(responseFind.body[0]).toHaveProperty("id");
    expect(responseFind.body[0].email).toEqual("patient@test.com");
  });

  it("must find a patient by phone", async () => {
    const responseAuth = await request(app)
      .post("/authentication/session")
      .send({
        email: "doctor@test.com",
        password: "123456",
      });

    const { access_token } = responseAuth.body;

    const responsePatient = await request(app)
      .post("/patient")
      .send({
        name: "Patient Test",
        phone: "(48) 9 8875-0125",
        email: "patient@test.com",
        birth_date: "1985-07-16",
        gender: "M",
        height: 1.67,
        weight: 70.4,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    const { phone } = responsePatient.body;

    const responseFind = await request(app)
      .get("/patient")
      .query({
        phone,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    expect(responseFind.status).toBe(200);
    expect(responseFind.body.length).toBe(1);
    expect(responseFind.body[0]).toHaveProperty("id");
    expect(responseFind.body[0].phone).toEqual("(48) 9 8875-0125");
  });

  it("should not find a patient with non-existent name", async () => {
    const responseAuth = await request(app)
      .post("/authentication/session")
      .send({
        email: "doctor@test.com",
        password: "123456",
      });

    const { access_token } = responseAuth.body;

    const responseFind = await request(app)
      .get("/patient")
      .query({
        name: "Teste2",
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    expect(responseFind.status).toBe(200);
    expect(responseFind.body.length).toBe(0);
  });

  it("should not find a patient with non-existent email", async () => {
    const responseAuth = await request(app)
      .post("/authentication/session")
      .send({
        email: "doctor@test.com",
        password: "123456",
      });

    const { access_token } = responseAuth.body;

    const responseFind = await request(app)
      .get("/patient")
      .query({
        email: "patientfind@test.com",
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    expect(responseFind.status).toBe(200);
    expect(responseFind.body.length).toBe(0);
  });

  it("should not find a patient with non-existent phone", async () => {
    const responseAuth = await request(app)
      .post("/authentication/session")
      .send({
        email: "doctor@test.com",
        password: "123456",
      });

    const { access_token } = responseAuth.body;

    const responseFind = await request(app)
      .get("/patient")
      .query({
        phone: "9 9905-6965",
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    expect(responseFind.status).toBe(200);
    expect(responseFind.body.length).toBe(0);
  });
});

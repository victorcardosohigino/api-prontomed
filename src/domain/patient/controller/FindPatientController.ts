import { Request, Response } from "express";
import { container } from "tsyringe";

import { FindPatientService } from "@domain/patient/service/FindPatientService";

class FindPatientController {
  async handle(request: Request, response: Response): Promise<Response> {
    const name = request.query.name as string;
    const phone = request.query.phone as string;
    const email = request.query.email as string;
    const doctor_id = request.doctor.doctor_id as string;

    const findPatientService = container.resolve(FindPatientService);

    const patients = await findPatientService.execute({
      doctor_id,
      name,
      phone,
      email,
    });

    return response.json(patients);
  }
}

export { FindPatientController };

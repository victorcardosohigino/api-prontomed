import { Request, Response } from "express";
import { container } from "tsyringe";

import { EditPatientService } from "@domain/patient/service/EditPatientService";

class EditPatientController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { id } = request.params;
    const { name, phone, email, birth_date, gender, height, weight } =
      request.body;
    const doctor_id = request.doctor.doctor_id as string;

    const editPatientService = container.resolve(EditPatientService);

    await editPatientService.execute({
      doctor_id,
      id,
      name,
      phone,
      email,
      birth_date,
      gender,
      height,
      weight,
    });

    return response.status(204).send();
  }
}

export { EditPatientController };

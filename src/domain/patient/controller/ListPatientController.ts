import { Request, Response } from "express";
import { container } from "tsyringe";

import { ListPatientService } from "@domain/patient/service/ListPatientService";

class ListPatientController {
  async handle(request: Request, response: Response): Promise<Response> {
    const offset = request.query.offset as string;
    const limit = request.query.limit as string;
    const doctor_id = request.doctor.doctor_id as string;

    const listPatientService = container.resolve(ListPatientService);

    const patients = await listPatientService.execute({
      doctor_id,
      offset: Number(offset),
      limit: Number(limit),
    });

    return response.json(patients);
  }
}

export { ListPatientController };

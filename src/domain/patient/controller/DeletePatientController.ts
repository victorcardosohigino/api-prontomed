import { Request, Response } from "express";
import { container } from "tsyringe";

import { DeletePatientService } from "@domain/patient/service/DeletePatientService";

class DeletePatientController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { id } = request.params;
    const doctor_id = request.doctor.doctor_id as string;

    const deletePatientService = container.resolve(DeletePatientService);

    await deletePatientService.execute({ id, doctor_id });

    return response.status(204).send();
  }
}

export { DeletePatientController };

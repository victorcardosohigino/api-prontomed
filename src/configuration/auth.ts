export default {
  secret_token: "46afd789y6541had987498a4gfdgfda8",
  expires_in_token: "1h",
  secret_refresh_token: "23yumk141kgh6874hfg654sed4gdsf87",
  expires_in_refresh_token: "30d",
  expires_refresh_token_days: 30,
};

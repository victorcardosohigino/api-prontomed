<p align="center">
  <a href="#sobre-o-projeto-">Sobre o Projeto</a> •
  <a href="#tecnologias-utilizadas-">Tecnologias utilizadas</a> •
  <a href="#features-">Features</a> •
  <a href="#executando-o-projeto-">Executando o projeto</a> •
  <a href="#executando-os-testes-">Executando os testes</a> •
  <a href="#documentação-">Documentação</a> •
  <a href="#banco-de-dados-">Banco de dados</a> •
  <a href="#histórico-de-releases-">Histórico de releases</a>
</p>

<p align="center">
  <a href="https://www.linkedin.com/in/victor-cardoso-higino/" target="_blank" rel="noopener noreferrer">
    <img alt="Made by" src="https://img.shields.io/badge/made%20by-Victor%20Cardoso%20de%20Higino-blue">
  </a>
	<img alt="License" src="https://img.shields.io/badge/License-MIT-yellow">
	<img alt="Version" src="https://img.shields.io/badge/Version-1.1.2-green">
</p>

## Sobre o projeto 📌

A API Prontomed possibilita o cadastro e manutenção de médicos, pacientes, agendamentos de consultas e prontuários médicos.

Documentação: [Documentação API](http://prontomed.ml/api-documentation/)

## Tecnologias utilizadas 📌

- Node.js • TypeScript • Express
- TypeORM • Tsyringe • uuid v4 • Day.js
- Eslint • Prettier • EditorConfig
- Jest • SuperTest
- JWT-token
- PostgreSQL
- Swagger
- Git • Gitlab
- Docker • Docker-compose
- Postman

## Features 📌
|                            |                    |
| -------------------------- | :----------------: |
| Cadastro de Medicos        |         ✔️         |
| Autenticação               |         ✔️         |
| Cadastro de Pacientes      |         ✔️         |
| Cadastro de Agendamentos   |         ✔️         |

## Executando o projeto 📌

**Clonando o projeto**

```bash
$ git clone https://gitlab.com/victorcardosohigino/api-prontomed.git

$ cd api-prontomed
```

**Alterando o arquivo .env**

```bash
$ cp .env.example .env
```

**Executando o build do projeto com docker-compose**

```bash
$ docker-compose up --build -d
```

**Executando as migrations para criação do banco de dados**

```bash
$ docker exec prontomed-api npm run typeorm migration:run
```

Para ver a API funcionando acesse **http://localhost:3333**

**Parando a execução do projeto**

```bash
$ docker-compose stop
```
> Para executar a API novamente executar o comando: `docker-compose up -d`

## Executando os testes 📌

> É necessário que a API esteja em execução para poder executar os testes

```bash
$ docker exec prontomed-api npm run test
```

## Documentação 📌

- Localhost: [Documentação Localhost](http://localhost:3333/api-documentation)
- Cloud: [Documentação Cloud](http://prontomed.ml/api-documentation/)
- Collection do Postman: [Collection do Postman](https://gitlab.com/victorcardosohigino/api-prontomed/-/blob/main/api-prontomed.postman_collection.json)
- Ambiente Local do Postman: [Ambiente Local do Postman](https://gitlab.com/victorcardosohigino/api-prontomed/-/blob/main/api-prontomed-local.postman_environment.json)
- Ambiente Cloud do Postman: [Ambiente Cloud do Postman](https://gitlab.com/victorcardosohigino/api-prontomed/-/blob/main/api-prontomed-cloud.postman_environment.json)

> Importe a collection no postman e depois os dois ambientes: local e cloud. Feito isso, irá estar disponível os dois ambientes: API Prontomed Local e API Prontomed Cloud.

- Diagrama ER: [Diagrama ER](https://gitlab.com/victorcardosohigino/api-prontomed/-/blob/main/diagrama-er.jpg)

## Banco de dados 📌

> Dados de acesso ao banco de dados local

Host: localhost<br>
Database: prontomed<br>
Username: prontomed<br>
Password: 123456

## Histórico de releases 📌
* 1.1.2 - Voltar para a porta padrão 5432 de teste
* 1.1.1 - Correção na porta de testes
* 1.1.0 - Alteração na execução do CI/CD
* 1.0.0 - Initial release

---

👍 Feito por Victor Cardoso Higino - &nbsp;[Linkedin](https://www.linkedin.com/in/victor-cardoso-higino/)
